var server = require('../../server/server');

module.exports = function (Coupon) {

    Coupon.applyCoupon = function (orderData, cb) {

        console.log(JSON.stringify(orderData));
        var cb = cb;

        var Coupon = server.models.Coupon;
        var Item = server.models.Item;
        var Category = server.models.Category;
        var Customer = server.models.Customer;

        var applyCoupon = function (value, coupon, innerCallBack) {

            console.log("values: " + value + ", " + JSON.stringify(coupon));

            var discountAmount = 0;
            var couponValue = coupon.value;
            var couponUnit = coupon.unit;

            if (couponUnit == 'percentage') {

                discountAmount = value * (couponValue / 100);

            } else if (couponUnit == 'amount') {
                discountAmount = couponValue;
            }

            if (coupon.noOfTimes > 0 && coupon.expiryTime > new Date() && coupon.startTime < new Date()) {

                if(discountAmount > coupon.maximumAmount){
                    discountAmount = coupon.maximumAmount;
                }
                var newNoOfTimes = coupon.noOfTimes - 1;
                coupon.updateAttributes({noOfTimes: newNoOfTimes}, function (err, couponUpdate) {
                    var couponInfo = {
                        "baseValue": value,
                        "discountAmount": discountAmount,
                        "finalValue": value - discountAmount
                    };
                    cb(null, couponInfo);
                });

            } else {

                var err = new Error("coupon is expired");
                cb(err, null);

            }

        }

        Coupon.find({'where': {'code': orderData.couponCode}}, function (err, coupons) {

            console.log(JSON.stringify(coupons));
            if (coupons.length > 0) {

                var coupon = coupons[0];

                if (coupon == null || coupon == undefined) {

                    var err = new Error("Invalid Coupon");
                    cb(err, null);

                } else {

                    var value = 0;
                    var couponType = coupon.assignedFor;

                    var couponApplied = false;
                    var cartValue = orderData.cartValue;
                    var products = orderData.products;

                    if (couponType == 'customer') {

                        Customer.findById(orderData.customerId, function (err, customer) {

                            if (customer) {
                                couponApplied = true;
                                value = cartValue;
                                applyCoupon(cartValue, coupon, cb);
                            } else if (err) {
                                cb(err, null);
                            } else {
                                var err = new Error("Customer is not available");
                                cb(err, null);
                            }

                        });

                    } else if (couponType == 'cartValue') {
                        couponApplied = true;
                        value = cartValue;
                        applyCoupon(cartValue, coupon, cb);
                    } else if (couponType == 'category' || coupon == 'product') {

                        if (products.length == 0) {
                            var err = new Error("products are not available");
                            cb(err, null);
                        }
                        for (var p in products) {

                            var product = products[p];

                            Item.findById(product.productId, function (err, item) {

                                if (item) {
                                    couponApplied = true;
                                    value = product.value;
                                    applyCoupon(cartValue, coupon, cb);
                                } else if (err) {
                                    cb(err, null);
                                } else {
                                    var err = new Error("Item is not available");
                                    cb(err, null);
                                }

                            });

                            Category.findById(product.categoryId, function (err, category) {

                                if (category) {
                                    couponApplied = true;
                                    value = product.value;
                                    applyCoupon(cartValue, coupon, cb);
                                } else if (err) {
                                    cb(err, null);
                                } else {
                                    var err = new Error("Category is not available");
                                    cb(err, null);
                                }

                            });

                        }

                    } else {
                        var err = new Error("Coupon is not applicable");
                        cb(err, null);
                    }

                }

            } else {
                var err = new Error("Invalid Coupon");
                cb(err, null);
            }

        });

    };

    Coupon.remoteMethod('applyCoupon', {
        description: "please give user details, we will process you coupon",
        returns: {
            arg: 'Coupon',
            type: 'array'
        },
        accepts: [{arg: 'orderData', type: 'object', http: {source: 'body'}}],
        http: {
            path: '/applyCoupon',
            verb: 'POST'
        }
    });

};
