var server = require('../../server/server');

module.exports = function (OrderRequest) {

    OrderRequest.observe('before save', function (ctx, next) {

        var orderRequestInfo;
        if (ctx.isNewInstance) {
            orderRequestInfo = ctx.instance;
        } else {
            orderRequestInfo = ctx.currentInstance;
        }

        console.log(JSON.stringify(orderRequestInfo));

        if (ctx.isNewInstance) {

            if (orderRequestInfo.type == 'product' && orderRequestInfo.operation == 'create') {

                var Product = server.models.Product;
                var Item = server.models.Item;

                Product.findById(orderRequestInfo.product, function (err, product) {

                        console.log('product: ' + JSON.stringify(product));

                        var prd = (JSON.parse(JSON.stringify(product)));

                        prd['productId'] = product.id;
                        delete prd['id'];

                        var ic = 0;

                        console.log('after: ' + JSON.stringify(prd));
                        console.log(orderRequestInfo.quantity);
                        for (var i = 0; i < orderRequestInfo.quantity; i++) {

                            console.log('init');
                            Item.create(prd, function (err, createdItem) {

                                if (err) {
                                    console.log('Error: ' + err);
                                }

                                console.log('create item: '+JSON.stringify(createdItem));

                                ic++;
                                if (ic == orderRequestInfo.quantity) {

                                    next();

                                }

                            });

                        }

                    }
                );

            } else if(orderRequestInfo.type == 'product' && orderRequestInfo.operation == 'move'){

                var Product = server.models.Product;
                var Item = server.models.Item;

                Product.findById(orderRequestInfo.product, function(err, product){

                    Item.find({'and':[{limit: orderRequestInfo.quantity},{'id':orderRequestInfo.product},{'storeId':orderRequestInfo.sourceStore}]}, function(err, items) {

                        var ic =0;
                        for(var i in items) {

                            var item = items[i];

                            item.updateAttributes({'storeId':orderRequestInfo.destinationStore}, function(err, item){

                                ic++;
                                if(ic == items.length){
                                    next();
                                }

                            });


                        }

                    });

                });

            } else {
                next();
            }

        } else {
            next();
        }

    });

};
