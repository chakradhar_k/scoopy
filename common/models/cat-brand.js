var server = require('../../server/server');

module.exports = function(CatBrand) {

    CatBrand.observe('loaded', function (ctx, next) {

        var Category = server.models.Category;
        var Brand = server.models.Brand;

        if(ctx.instance != undefined){

            var catbrand = ctx.instance;

            Category.findById(catbrand.categoryId, function(err, category){

                ctx.instance['category'] = category;
                Brand.find({}, function(err, brands){

                    for(var b in brands){

                        var brand = brands[b];
                        if(brand.id == catbrand.brandId) {
                            console.log("brand: "+brand.id);
                            ctx.instance['brand'] = brand;
                            //console.log("cb : "+ JSON.stringify(ctx.instance));
                            next();
                            break;
                        }

                    }


                });

            });

        } else {
            next();
        }

    });

};
