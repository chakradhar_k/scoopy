var server = require('../../server/server');

module.exports = function (Customer) {

    Customer.validatesUniquenessOf('phone', {message: 'Phone could be unique'});

    Customer.observe('before save', function (ctx, next) {

        console.log('before saving');

        var Customer = server.models.Customer;

        console.log("instance: " + JSON.stringify(ctx.instance));
        if (ctx.instance) {

            //console.log("entering: " + JSON.stringify(ctx.instance));

            Customer.find({"where": {"id": ctx.instance.id}}, function (err, customers) {

                if (customers.length > 0) {
                    next();
                } else {
                    console.log("into phone user");
                    var randomstring = require("randomstring");

                    var otp = randomstring.generate({
                        length: 4,
                        charset: 'numeric'
                    });


                    //ctx.instance.otp = otp;
                    //ctx.instance.otpStatus = false;

                    if (ctx.instance.email == undefined) {
                        ctx.instance.email = ctx.instance.phone.toString() + "@scoopy.com";
                    }
                    console.log("before save: " + JSON.stringify(ctx.instance));

                    var EmailN = server.models.EmailN;
                    //var SMS = server.models.SMS;
                    //
                    //SMS.create({destination: ctx.instance.phone, message: 'your one time password is ' + otp}, function (err, sms) {
                    //    console.log(sms);
                    //});

                    EmailN.create({
                        "to": ctx.instance.email,
                        "from": "info@scoopy.com",
                        "subject": "Your OTP",
                        "text": "",
                        "html": "your one time password is " + ctx.instance.otp
                    }, function (err, email) {
                        console.log(email);
                    });
                    next();
                }

            });
        } else {
            console.log(JSON.stringify(ctx));
            next();
        }
    });

    Customer.validateOTP = function (phone, otp, cb) {

        var cb = cb;
        var Customer = server.models.Customer;
        var SMS = server.models.SMS;
        var EmailN = server.models.EmailN;

        Customer.findOne({"where": {"phone": phone}}, function (err, customer) {

            //console.log("customer: "+ JSON.stringify(customer));
            if (err) {
                cb(null, err);
            } else if (customer == undefined) {

                var error = new Error("Your are not registered to our application");
                cb(error, null);

            } else {
                if (customer.otp == otp) {

                    customer.updateAttributes({otpStatus: true}, function (err, customerNext) {

                        var smsMessage = 'Hi! Thank you for downloading and registering';
                        var destination = customerNext.phone;

                        SMS.create({destination: destination, message: smsMessage}, function (err, sms) {

                        });

                        cb(null, customerNext);
                    });

                } else {
                    var error = new Error("OTP is not matched");
                    cb(error, null);
                    //next();
                }
            }

        });

    };

    Customer.remoteMethod('validateOTP', {
        description: "Enter your otp to activate account",
        returns: {
            arg: 'customer',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
            {arg: 'otp', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/validateOTP',
            verb: 'get'
        }
    });

    Customer.resendOTP = function (phone, cb) {

        var cb = cb;

        var Customer = server.models.Customer;
        var SMS = server.models.SMS;
        var EmailN = server.models.EmailN;

        Customer.findOne({"where": {"phone": phone}}, function (err, customer) {

            if (err) {
                cb(null, err);
            } else if (customer == undefined) {

                var error = new Error("New Registration");
                cb(error, null);

            } else {
                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 4,
                    charset: 'numeric'
                });
                ;

                var SMS = server.models.SMS;

                var smsMessage = 'Your unique verification code for Scoopy is ' + otp + ' Thank you';
                var destination = customer.phone;


                customer.updateAttributes({otpStatus: false, otp: otp}, function (err, customerNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });

                });
            }

        });

    };

    Customer.remoteMethod('resendOTP', {
        description: "Enter your otp to activate account",
        returns: {
            arg: 'customer',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/resendOTP',
            verb: 'get'
        }
    });

    Customer.forgotPassword = function (phone, cb) {

        var cb = cb;

        var Customer = server.models.Customer;
        var SMS = server.models.SMS;
        var EmailN = server.models.EmailN;

        Customer.findOne({"where": {"phone": phone}}, function (err, customer) {

            if (err) {
                cb(null, err);
            } else if (customer == undefined) {

                var error = new Error("Your are not registered to our application");
                cb(error, null);

            } else {
                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 4,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                var smsMessage = 'Your password is ' + otp + ' Thank you';
                var destination = customer.phone;


                customer.updateAttributes({"password": otp}, function (err, customerNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });

                });
            }

        });

    };

    Customer.remoteMethod('forgotPassword', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'customer',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/forgotPassword',
            verb: 'get'
        }
    });


    Customer.userExists = function (phone, cb) {

        var cb = cb;

        var Customer = server.models.Customer;

        Customer.find({"where": {"phone": phone}}, function (err, customers) {

            if (err) cb(err, null);

            if (customers.length > 0) {

                var customer = customers[0];

                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 4,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                SMS.create({destination: phone, message: 'your one time password is ' + otp}, function (err, sms) {
                    console.log(sms);
                });

                customer.updateAttributes({"otp": otp, "password": otp}, function (err, customerUpdate) {
                    cb(null, customerUpdate);
                })

            } else {

                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 4,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                SMS.create({destination: phone, message: 'your one time password is ' + otp}, function (err, sms) {
                    console.log(sms);
                });

                Customer.create({
                    "phone": phone,
                    "otp": otp,
                    "email": phone + '@scoopy.com',
                    "otpStatus": false,
                    "password": otp,
                    "name": "",
                    "loyalty": {"totalPoints": 100, "points": 100, "amount": 1, "currency": "Rupee"}
                }, function (err, customerUpdate) {
                    console.log("creating customer");
                    cb(null, customerUpdate);
                })
                //var err = new Error("User information is not available");
                //cb(err, null);
            }

        });

    };

    Customer.remoteMethod('userExists', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'customer',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/userExists',
            verb: 'get'
        }
    });

};
