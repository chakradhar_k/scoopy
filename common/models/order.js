var server = require('../../server/server');
module.exports = function(Order) {

    Order.observe('before save', function (ctx, next) {

        var Customer = server.models.Customer;
        console.log("in: "+ JSON.stringify(ctx.data));
        console.log("Instance:"+JSON.stringify(ctx.instance));
        console.log(JSON.stringify(ctx.currentInstance));

        var orderInfo;
        if(ctx.isNewInstance) {
            var randomstring = require("randomstring");

            var orderId = randomstring.generate({
                length: 6,
                charset: 'numeric'
            });

            ctx.instance['orderId'] = orderId;
            orderInfo = ctx.instance;
        } else {
            orderInfo = ctx.currentInstance;
        }

        console.log("oi: "+JSON.stringify(orderInfo));

        if((ctx.instance != undefined) || (ctx.data != undefined)) {

            var data = ctx.instance || ctx.data;

            if(data.paymentStatus == true || data.paymentStatus == 'true') {

                var cartValueChange = parseInt(orderInfo.cartValue);
                console.log("cartValue:"+cartValueChange);
                cartValueChange = parseInt(cartValueChange/100);
                console.log("cartValue:"+cartValueChange);
                var newlPoints = 0;

                if(cartValueChange <= 2) {
                    newlPoints = newlPoints+ (cartValueChange * 5);
                } else if(cartValueChange > 2 && cartValueChange < 4) {
                    newlPoints = newlPoints+ (cartValueChange * 10);
                } else if(cartValueChange >= 4 && cartValueChange < 10) {
                    newlPoints = newlPoints+ (cartValueChange * 20);
                } else {
                    newlPoints = newlPoints+ (cartValueChange * 25);
                }

                console.log("New Points:"+newlPoints);
                console.log(orderInfo.loyaltyPoint);
                var loyaltyPoint = parseInt(orderInfo.loyaltyPoint);
                if(loyaltyPoint == undefined) {
                    loyaltyPoint = 0;
                }

                console.log(loyaltyPoint);

                console.log("console: "+ JSON.stringify(orderInfo));

                Customer.findById(orderInfo.customerId, function(err, customerR){
                    console.log('enters:');
                    if(customerR != undefined) {
                        var loyalty = customerR.loyalty;
                        console.log(loyaltyPoint);
                        console.log(loyalty.totalPoints);
                        loyalty.totalPoints = parseInt(loyalty.totalPoints) + newlPoints - parseInt(loyaltyPoint);
                        if(loyalty.totalPoints < 0){
                            loyalty.totalPoints = 0;
                        }
                        console.log('loyal: '+JSON.stringify(loyalty));
                        loyalty.amount = loyalty.totalPoints/100;
                        customerR.updateAttributes({"loyalty":loyalty}, function(err, customerU){

                            next();

                        });
                    } else {
                        next();
                    }
                });

            } else {
                next();
            }
        }

    });

};
