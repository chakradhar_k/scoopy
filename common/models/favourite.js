module.exports = function(Favourite) {

    Favourite.validatesUniquenessOf('fuid', {message: 'Already added to favourites'});

};
