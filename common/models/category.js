var server = require('../../server/server');

module.exports = function(Category) {

    Category.getFlavoursAndBrands = function (categoryId, cb) {

        var cb = cb;

        var Category = server.models.Category;
        var CatFla = server.models.CatFla;
        var CatBrand = server.models.CatBrand;

        Category.findOne({"where":{"id":categoryId}}, function(err, category){

            if(category != undefined) {
                var flavours = [];
                var c =0;
                CatFla.find({"where":{"categoryId": categoryId}}, function (err, catflas) {

                    for(var cf in catflas){

                        var catfla = catflas[cf];
                        console.log(catfla);
                        if(catfla != null) {
                            flavours.push(catfla.flavour);
                        }

                    }
                    c++;
                    category.flavours = flavours;
                    if(c == 2){
                        cb(null, category);
                    }

                });

                var brands = [];
                CatBrand.find({"where":{"categoryId": categoryId}}, function (err, catbrands) {

                    for(var cf in catbrands){

                        var catbrand = catbrands[cf];
                        console.log(catbrand);
                        if(catbrand != null){
                            brands.push(catbrand.brand);
                        }

                    }
                    c++;
                    category.brands = brands;
                    if(c == 2){
                        cb(null, category);
                    }

                });
            } else {
                var err = Error("Not found category");
                cb(err, null);
            }

            //var cusCategory = {};
            //cusCategory['id'] = categoryId;
            //Flavour.find({'where':{'categoryId': categoryId}}, function (err, flavours) {
            //
            //    cusCategory.flavours = flavours;
            //    //console.log(JSON.stringify(flavours));
            //    Brand.find({'where':{'categoryId': categoryId}}, function(err, brands){
            //
            //        cusCategory.brands = brands;
            //        //console.log(JSON.stringify(cusCategory));
            //        cb(null, cusCategory);
            //
            //    });
            //
            //});

        });

    };

    Category.remoteMethod('getFlavoursAndBrands', {
        description: "Enter category id to get all flavours and brands",
        returns: {
            arg: 'category',
            type: 'string'
        },
        accepts: [{arg: 'categoryId', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/getFlavoursAndBrands',
            verb: 'get'
        }
    });

    Category.ci = function (cb) {

        var cb = cb;
        var Category = server.models.Category;

        Category.find({}, function(err, categories){

            var i =0
            for(var c in categories){

                var category = categories[c];

                category.updateAttributes({"name":category['name ']}, function(err, categoryUpdate){

                    i++;
                    if(i = categories.length) {

                        cb(null, categories);

                    }

                });

            }

        });


    };

    Category.remoteMethod('ci', {
        description: "ci",
        returns: {
            arg: 'items',
            type: 'array'
        },
        accepts: [],
        http: {
            path: '/ci',
            verb: 'get'
        }
    });

};
