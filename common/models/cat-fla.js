var server = require('../../server/server');

module.exports = function(CatFla) {

    CatFla.observe('loaded', function (ctx, next) {

        var Category = server.models.Category;
        var Flavour = server.models.Flavour;

        if(ctx.instance != undefined){

            var catfla = ctx.instance;

            Category.findById(catfla.categoryId, function(err, category){

                Flavour.findById(catfla.flavourId, function(err, flavour){

                    ctx.instance['category'] = category;
                    ctx.instance['flavour'] = flavour;
                    console.log("cf : "+ JSON.stringify(ctx.instance));
                    next();

                });

            });

        } else {
            next();
        }

    });

};
