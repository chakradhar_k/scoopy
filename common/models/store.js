var server = require('../../server/server');
//var geolib = require('geolib');
var geodist = require('geodist');

module.exports = function (Store) {

    Store.validatesUniquenessOf('phone', {message: 'Phone could be unique'});

    Store.observe('before save', function (ctx, next) {

        var Store = server.models.Store;

        if (ctx.instance) {

            console.log("entering: " + JSON.stringify(ctx.instance));

            Store.find({"where": {"id": ctx.instance.id}}, function (err, stores) {

                if (stores.length > 0) {

                } else {
                    console.log("into phone user");
                    var randomstring = require("randomstring");

                    var otp = randomstring.generate({
                        length: 4,
                        charset: 'numeric'
                    });


                    //ctx.instance.otp = otp;
                    //ctx.instance.otpStatus = false;

                    if (ctx.instance.email == undefined) {
                        ctx.instance.email = ctx.instance.phone.toString() + "@scoopy.com";
                    }
                    console.log("before save: " + JSON.stringify(ctx.instance));

                    var EmailN = server.models.EmailN;
                    //var SMS = server.models.SMS;
                    //
                    //SMS.create({destination: ctx.instance.phone, message: 'your one time password is ' + otp}, function (err, sms) {
                    //    console.log(sms);
                    //});

                    EmailN.create({
                        "to": ctx.instance.email,
                        "from": "info@scoopy.com",
                        "subject": "Your OTP",
                        "text": "",
                        "html": "your one time password is " + ctx.instance.otp
                    }, function (err, email) {
                        console.log(email);
                    });

                }
                next();
            });
        } else {
            next();
        }

    });

    Store.getNearestStoreInformation = function (lat, lng, cb) {

        var redis = require('redis'),
            client = redis.createClient()

        var geo = require('georedis').initialize(client);

        console.log("into");
        var cb = cb;
        var nearestStores = [];
        var Address = server.models.Address;
        Store.find({}, function (err, stores) {

            var locations = [];

            console.log("stores length: " + stores.length);

            if (err) cb(null, err);
            var sc = 0;
            for (var s in stores) {

                var store = stores[s];

                Address.find({"where": {"storeId": store.id, "type": "Store"}}, store, function (err, addresses) {

                    var store = this.store;
                    var storeAddress = [];
                    console.log("store add: " + JSON.stringify(store) + "-->" + JSON.stringify(addresses));

                    sc++;
                    if (err) {
                        //cb(null, err)
                    } else if (addresses.length > 0) {
                        var address = addresses[0];
                        console.log("address: " + JSON.stringify(address));
                        //storeAddress.push(address.geoLocation.lat);
                        //storeAddress.push(address.geoLocation.lng);
                        //storeAddress.push(store.id);
                        //console.log(storeAddress);
                        geo.addLocation(store.id.toString(), {
                            latitude: address.geoLocation.lat,
                            longitude: address.geoLocation.lng
                        }, function (err, reply) {
                            if (err) console.error(err)
                            else console.log('added location:', reply);
                        })
                        //locations.push(JSON.stringify(storeAddress));
                        //var distance = geodist({lat: address.geoLocation.lat, lon: address.geoLocation.lng}, {lat: lat, lon: lng}, {format: true, unit: 'km'});
                        //var distance = geolib.getDistance(
                        //    {latitude: lat, longitude: lng},
                        //    {latitude: address.geoLocation.lat, longitude: address.geoLocation.lng}
                        //);

                    } else {
                        //cb(null, []);
                    }

                    if (sc == stores.length) {
                        //console.log(JSON.stringify(locations));

                        //console.log(geo.);

                        var options = {
                            withCoordinates: true, // Will provide coordinates with locations, default false
                            withHashes: true, // Will provide a 52bit Geohash Integer, default false
                            withDistances: true, // Will provide distance from query, default false
                            order: 'ASC', // or 'DESC' or true (same as 'ASC'), default false
                            units: 'm', // or 'km', 'mi', 'ft', default 'm'
                            count: 100, // Number of results to return, default undefined
                            accurate: true // Useful if in emulated mode and accuracy is important, default false
                        }

                        geo.nearby({latitude: lat, longitude: lng}, 5000, options, function (err, locationsReturn) {
                            //if(err) console.error(err)
                            //else console.log('nearby locations:', locations)

                            console.log(JSON.stringify(locationsReturn));
                            var storesR = [];
                            var cl = 0;
                            var sc = 0;
                            if (locationsReturn.length == 0) {
                                var err = new Error("no store is available");
                                cb(err, null);
                            } else {
                                for (var l in locationsReturn) {

                                    var locationS = locationsReturn[l];
                                    console.log(JSON.stringify(locationS));

                                    Store.findById(locationS.key, function (err, storeN) {

                                        cl++;
                                        if (storeN) {
                                            if (sc == 0) {
                                                storesR.push(storeN);
                                                sc++;
                                            }
                                        }
                                        if (cl == locationsReturn.length) {
                                            geo.delete(function (err) {

                                            });
                                            cb(null, storesR);
                                        }


                                    });

                                }
                            }
                        });
                        //proximity.addLocations(locations, function(err, reply){
                        //    if(err) console.error(err)
                        //    else console.log('added locations:', reply)
                        //    // look for all points within ~5000m of Toronto.
                        //
                        //})
                    }


                }.bind({store: store}));

            }

            if (stores.length == 0) {
                cb(null, []);
            }

        });

    }

    Store.remoteMethod('getNearestStoreInformation', {
        description: "get nearest store information in 7km radius",
        returns: {
            arg: 'stores',
            type: 'array'
        },
        accepts: [{arg: 'lat', type: 'string', http: {source: 'query'}},
            {arg: 'lng', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/getNearestStoreInformation',
            verb: 'get'
        }
    });


    Store.validateOTP = function (phone, otp, cb) {

        var cb = cb;
        var Store = server.models.Store;
        var SMS = server.models.SMS;
        var EmailN = server.models.EmailN;

        Store.findOne({"where": {"phone": phone}}, function (err, store) {

            //console.log("store: "+ JSON.stringify(store));
            if (err) {
                cb(null, err);
            } else if (store == undefined) {

                var error = new Error("Your are not registered to our application");
                cb(error, null);

            } else {
                if (store.otp == otp) {

                    store.updateAttributes({otpStatus: true}, function (err, storeNext) {

                        var smsMessage = 'Hi! Thank you for downloading and registering';
                        var destination = storeNext.phone;

                        SMS.create({destination: destination, message: smsMessage}, function (err, sms) {

                        });

                        cb(null, storeNext);
                    });

                } else {
                    var error = new Error("OTP is not matched");
                    cb(error, null);
                    //next();
                }
            }

        });

    };

    Store.remoteMethod('validateOTP', {
        description: "Enter your otp to activate account",
        returns: {
            arg: 'store',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
            {arg: 'otp', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/validateOTP',
            verb: 'get'
        }
    });

    Store.resendOTP = function (phone, cb) {

        var cb = cb;

        var Store = server.models.Store;
        var SMS = server.models.SMS;
        var EmailN = server.models.EmailN;

        Store.findOne({"where": {"phone": phone}}, function (err, store) {

            if (err) {
                cb(null, err);
            } else if (store == undefined) {

                var error = new Error("Your are not registered to our application");
                cb(error, null);

            } else {
                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 4,
                    charset: 'numeric'
                });
                ;

                var SMS = server.models.SMS;

                var smsMessage = 'Your unique verification code for Scoopy is ' + otp + ' Thank you';
                var destination = store.phone;


                store.updateAttributes({otpStatus: false, otp: otp}, function (err, storeNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });

                });
            }

        });

    };

    Store.remoteMethod('resendOTP', {
        description: "Enter your otp to activate account",
        returns: {
            arg: 'store',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/resendOTP',
            verb: 'get'
        }
    });

    Store.forgotPassword = function (phone, cb) {

        var cb = cb;

        var Store = server.models.Store;
        var SMS = server.models.SMS;
        var EmailN = server.models.EmailN;

        Store.findOne({"where": {"phone": phone}}, function (err, store) {

            if (err) {
                cb(null, err);
            } else if (store == undefined) {

                var error = new Error("Your are not registered to our application");
                cb(error, null);

            } else {
                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                var smsMessage = 'Your password is ' + otp + ' Thank you';
                var destination = store.phone;


                store.updateAttributes({"password": otp}, function (err, storeNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });

                });
            }

        });

    };

    Store.remoteMethod('forgotPassword', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'store',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/forgotPassword',
            verb: 'get'
        }
    });

    Store.userExists = function (phone, cb) {

        var cb = cb;

        Store.find({"where": {"phone": phone}}, function (err, stores) {

            if (err) cb(err, null);

            if (stores.length > 0) {

                var store = stores[0];

                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 4,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                SMS.create({destination: phone, message: 'your one time password is ' + otp}, function (err, sms) {
                    console.log(sms);
                });

                store.updateAttributes({"otp": otp, "password": otp}, function (err, storeUpdate) {
                    cb(null, storeUpdate);
                })

            } else {

                console.log("New Store");
                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 4,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                SMS.create({destination: phone, message: 'your one time password is ' + otp}, function (err, sms) {
                    console.log(sms);
                });

                var Store = server.models.Store;
                Store.create({
                    "phone": phone,
                    "otp": otp,
                    "email": phone + '@scoopy.com',
                    "otpStatus": false,
                    "password": otp,
                    "name": ""
                }, function (err, storeUpdate) {
                    if (err) console.log("error: " + err);
                    cb(null, storeUpdate);
                })
                //var err = new Error("User information is not available");
                //cb(err, null);
            }

        });

    };

    Store.remoteMethod('userExists', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'store',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/userExists',
            verb: 'get'
        }
    });

};
