var server = require('../../server/server');

module.exports = function(Loyalty) {

    Loyalty.manageLoyalty = function (customerId, operationType, value, cb) {

        var cb = cb;

        var Customer = server.models.Customer;

        Customer.findById(customerId, function(err, customer){

            if(err) {
                cb(err, null);
            } else {
                var loyalty = customer.loyalty;

                var loyaltyPoints = loyalty.totalPoints;

                if(operationType == 'Add'){

                    loyaltyPoints = parseInt(loyaltyPoints) + parseInt(value);

                } else if(operationType == 'Remove') {

                    loyaltyPoints = parseInt(loyaltyPoints) - parseInt(value);

                }

                loyalty.totalPoints = loyaltyPoints;

                customer.updateAttributes({"loyalty":loyalty}, function(err, customerUpdate){

                    if(err) {
                        cb(err, null);
                    } else {
                        cb(null, customerUpdate);
                    }

                });
            }

        });

    };

    Loyalty.remoteMethod('manageLoyalty', {
        description: "please update payment status",
        returns: {
            arg: 'loyalty',
            type: 'array'
        },
        accepts: [{arg: 'customerId', type: 'string', http: {source: 'query'}},
            {arg: 'operationType', type: 'string', http: {source: 'query'}},
            {arg: 'value', type: 'string', http: {source: 'query'}}
        ],
        http: {
            path: '/manageLoyalty',
            verb: 'get'
        }
    });

};
