var server = require('../../server/server');

module.exports = function(Item) {


    Item.observe('loaded', function(ctx, next){

        if(ctx.instance) {
            var Category = server.models.Category;
            var Brand = server.models.Brand;
            var Flavour = server.models.Flavour;
            var Review = server.models.Review;

            var item = ctx.instance;

            Category.findById(item.categoryId, function(err, category){

                ctx.instance['category'] = category;
                Brand.find({}, function(err, brands){

                    for(var b in brands){

                        var brand = brands[b];
                        if(brand.id == item.brandId) {
                            console.log("brand: "+brand.id);
                            ctx.instance['brand'] = brand;
                            //console.log("cb : "+ JSON.stringify(ctx.instance));
                            //next();
                            break;
                        }

                    }
                    Flavour.findById(item.flavourId, function(err, flavour){

                        ctx.instance['flavour'] = flavour;

                        Review.find({"where":{"ratingFor":item.id}}, function(err, reviews){

                            var topReviews = [];
                            var overallRating = 0;
                            var count = 0;
                            console.log("Reviews..."+JSON.stringify(reviews));
                            for(var k=0; k<5; k++) {

                                var reviewI = reviews[k];
                                if(reviewI != null && reviewI != undefined) {
                                    //console.log("Review Info:"+JSON.stringify(reviewI));
                                    overallRating = overallRating + reviewI.rating;
                                    count = count+1;
                                    topReviews.push(reviewI);
                                }

                            }

                            var avgRating = 0;
                            if(overallRating > 0){
                                avgRating = overallRating/count;
                            }
                            console.log("reviews rating and count...."+overallRating+"...."+count+'.....avg:'+avgRating);
                            ctx.instance['reviews'] = topReviews;
                            ctx.instance['avgRating'] = avgRating;
                            ctx.instance['no_ofReviews'] = count;
                            next();

                        });

                    });

                });

            });
        } else {
            next();
        }


    });

    Item.customSearch = function (filterData, cb) {

        console.log(filterData);
        var categoryId = filterData.categoryId;
        var minPrice = filterData.minPrice;
        var maxPrice = filterData.maxPrice;
        var flavours = filterData.flavours;
        var brands = filterData.brands;

        var cb = cb;
        var Item = server.models.Item;

        if(categoryId != undefined) {
            //filterCompose.push({'categoryId': categoryId});
            var cataId = categoryId;
        }

        var flavourArray = [];
        if(flavours  != undefined) {
            if(flavours.length>0) {
                for (var i = 0; i < flavours.length; i++) {
                    flavourArray.push({"flavourId": flavours[i]});
                }
            }else{
                flavourArray = [""];
            }
        }

        var brandArray = [];
        if(brands  != undefined) {
            if(brands.length > 0) {
                for (var i = 0; i < brands.length; i++) {
                    brandArray.push({"brandId": brands[i]});
                }
            }else{
                brandArray = [""];
            }
            //filterCompose.push({'or':  brandArray});
        }

        var filterCompose = [
            {
                "or": flavourArray
            },
            {
                "or": brandArray
            },
            {
                "categoryId": cataId
            }
        ];

        if(minPrice != undefined) {
            filterCompose.push({'price': {gte: Number(minPrice)}});
        }
        if(maxPrice != undefined) {
            filterCompose.push({'price': {lte: Number(maxPrice)}});
        }

        console.log("fc: "+JSON.stringify(filterCompose));
        Item.find({"where":{"and":filterCompose}}, function(err, items) {

            cb(null, items);

        });


    };

    Item.remoteMethod('customSearch', {
        description: "customSearch",
        returns: {
            arg: 'items',
            type: 'array'
        },
        accepts: [{arg: 'filterData', type: 'object', http: {source: 'body'}}],
        http: {
            path: '/customSearch',
            verb: 'POST'
        }
    });

    Item.ci = function (cb) {

        var cb = cb;
        var Item = server.models.Item;

        Item.find({}, function(err, items) {

            //console.log(JSON.stringify(items));
            var ic = 0;
            for(var i in items){
                var item = items[i];
                console.log("ci: "+ item.categoryId);
                if(item.categoryId.toString() == '56cee6e8c4b8f7197ced15d0'){

                    item.updateAttribute("categoryId", "56cff1dac4b8f7197ced1728", function(err, itemU){

                        ic++;
                        if(ic == items.length){
                            cb(null, items);
                        }

                    });

                }

            }

        });


    };

    Item.remoteMethod('ci', {
        description: "ci",
        returns: {
            arg: 'items',
            type: 'array'
        },
        accepts: [],
        http: {
            path: '/ci',
            verb: 'GET'
        }
    });

};
