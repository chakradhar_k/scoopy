var server = require('../../server/server');

module.exports = function (PhoneUser) {

    PhoneUser.validatesUniquenessOf('phone', {message: 'Phone could be unique'});

    //PhoneUser.observe('access', function (ctx, next) {
    //
    //    console.log(JSON.stringify(ctx));
    //    next();
    //
    //});

    //PhoneUser.observe('loaded', function (ctx, next) {
    //
    //    console.log("loaded: "+JSON.stringify(ctx));
    //    next();
    //
    //});

    //PhoneUser.beforeRemote('login', function(ctx, phoneUser, next){
    //
    //    console.log('login: '+ JSON.stringify(ctx));
    //    next();
    //
    //});

    PhoneUser.observe('before save', function (ctx, next) {

        var PhoneUser = server.models.PhoneUser;

        if(ctx.instance){

            console.log("entering: "+ JSON.stringify(ctx.instance));

            PhoneUser.find({"where":{"id":ctx.instance.id}}, function(err, phoneUsers){

                if(phoneUsers.length>0) {

                } else {
                    console.log("into phone user");
                    var randomstring = require("randomstring");

                    var otp = randomstring.generate({
                        length: 6,
                        charset: 'numeric'
                    });


                    //ctx.instance.otp = otp;
                    //ctx.instance.otpStatus = false;

                    if (ctx.instance.email == undefined) {
                        ctx.instance.email = ctx.instance.phone.toString() + "@scoopy.com";
                    }
                    console.log("before save: " + JSON.stringify(ctx.instance));

                    var EmailN = server.models.EmailN;
                    //var SMS = server.models.SMS;
                    //
                    //SMS.create({destination: ctx.instance.phone, message: 'your one time password is ' + otp}, function (err, sms) {
                    //    console.log(sms);
                    //});

                    EmailN.create({
                        "to": ctx.instance.email,
                        "from": "info@scoopy.com",
                        "subject": "Your OTP",
                        "text": "",
                        "html": "your one time password is " + ctx.instance.otp
                    }, function (err, email) {
                        console.log(email);
                    });

                }
                next();
            });
        } else {
            next();
        }

    });


    //PhoneUser.validateOTP = function (phone, otp, cb) {
    //
    //    var cb = cb;
    //    var PhoneUser = server.models.PhoneUser;
    //    var SMS = server.models.SMS;
    //    var EmailN = server.models.EmailN;
    //
    //    PhoneUser.findOne({"where": {"phone": phone}}, function (err, phoneUser) {
    //
    //        if (err) {
    //            cb(null, err);
    //        }
    //
    //        if (phoneUser.otp == otp) {
    //
    //            phoneUser.updateAttributes({otpStatus: true}, function (err, phoneUserNext) {
    //
    //                var smsMessage = 'Hi! Thank you for downloading and registering on Ouroneapp, now keep adding your Local retailers to get updates on what’s new in their store';
    //                var destination = buyerNext.phone;
    //
    //                SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
    //
    //                });
    //
    //                //var fs = require('fs');
    //                //
    //                //fs.readFile(__dirname + '/../../client/htmltemplates/BuyerConfirmation.html', 'utf8', function (err, data) {
    //                //    if (err) {
    //                //        return console.log(err);
    //                //    }
    //                //
    //                //    data = data.replace('$userName$', buyerNext.name);
    //                //
    //                //    EmailN.create({
    //                //        to: buyerNext.email,
    //                //        from: 'info@ouroneapp.com',
    //                //        subject: 'Registration Success',
    //                //        text: '',
    //                //        html: data
    //                //    }, function (err, email) {
    //                //
    //                //    });
    //                //
    //                //    EmailN.create({
    //                //        to: 'info@ouroneapp.com',
    //                //        from: 'info@ouroneapp.com',
    //                //        subject: 'Registration Success',
    //                //        text: '',
    //                //        html: data
    //                //    }, function (err, email) {
    //                //
    //                //    });
    //                //    //console.log(data);
    //                //});
    //
    //
    //                cb(null, phoneUserNext);
    //            });
    //
    //        } else {
    //            cb(null, null);
    //            //next();
    //        }
    //
    //    });
    //
    //};
    //
    //PhoneUser.remoteMethod('validateOTP', {
    //    description: "Enter your otp to activate account",
    //    returns: {
    //        arg: 'phoneUser',
    //        type: 'array'
    //    },
    //    accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
    //        {arg: 'otp', type: 'string', http: {source: 'query'}}],
    //    http: {
    //        path: '/validateOTP',
    //        verb: 'get'
    //    }
    //});
    //
    //PhoneUser.resendOTP = function (phone, cb) {
    //
    //    var cb = cb;
    //
    //    var PhoneUser = server.models.PhoneUser;
    //    var SMS = server.models.SMS;
    //    var EmailN = server.models.EmailN;
    //
    //    PhoneUser.findOne({"where": {"phone": phone}}, function (err, phoneUser) {
    //
    //        if (err) {
    //            cb(null, err);
    //        }
    //
    //        var randomstring = require("randomstring");
    //
    //        var otp = randomstring.generate({
    //            length: 6,
    //            charset: 'numeric'
    //        });
    //        ;
    //
    //        var SMS = server.models.SMS;
    //
    //        var smsMessage = 'Your unique verification code for Ouroneapp is ' + otp + ' Thank you';
    //        var destination = phoneUser.phone;
    //
    //
    //        phoneUser.updateAttributes({otpStatus: false, otp: otp}, function (err, phoneUserNext) {
    //
    //            SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
    //                cb(null, sms);
    //            });
    //
    //        });
    //
    //    });
    //
    //};
    //
    //PhoneUser.remoteMethod('resendOTP', {
    //    description: "Enter your otp to activate account",
    //    returns: {
    //        arg: 'phoneUser',
    //        type: 'array'
    //    },
    //    accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
    //    http: {
    //        path: '/resendOTP',
    //        verb: 'get'
    //    }
    //});
    //
    //PhoneUser.forgotPassword = function (phone, cb) {
    //
    //    var cb = cb;
    //
    //    var PhoneUser = server.models.PhoneUser;
    //    var SMS = server.models.SMS;
    //    var EmailN = server.models.EmailN;
    //
    //    PhoneUser.findOne({"where": {"phone": phone}}, function (err, phoneUser) {
    //
    //        if (err) {
    //            cb(null, err);
    //        }
    //
    //        var randomstring = require("randomstring");
    //
    //        var otp = randomstring.generate({
    //            length: 6,
    //            charset: 'numeric'
    //        });
    //
    //        var SMS = server.models.SMS;
    //
    //        var smsMessage = 'Your password is ' + otp + ' Thank you';
    //        var destination = phoneUser.phone;
    //
    //
    //        phoneUser.updateAttributes({"password": otp}, function (err, phoneUserNext) {
    //
    //            SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
    //                cb(null, sms);
    //            });
    //
    //        });
    //
    //    });
    //
    //};
    //
    //PhoneUser.remoteMethod('forgotPassword', {
    //    description: "Enter mobile number and user type to reset the password",
    //    returns: {
    //        arg: 'phoneUser',
    //        type: 'array'
    //    },
    //    accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
    //    http: {
    //        path: '/forgotPassword',
    //        verb: 'get'
    //    }
    //});


    //PhoneUser.userExists = function (phone, cb) {
    //
    //    var cb = cb;
    //
    //    var PhoneUser = server.models.PhoneUser;
    //
    //    PhoneUser.find({"where":{"phone":phone}}, function(err, phoneUsers){
    //
    //        if(err) cb(err, null);
    //
    //        if(phoneUsers.length > 0) {
    //
    //            var phoneUser = phoneUsers[0];
    //
    //            cb(null, phoneUser);
    //
    //        } else {
    //            var err = new Error("User information is not available");
    //            cb(err, null);
    //        }
    //
    //    });
    //
    //};

    //PhoneUser.remoteMethod('userExists', {
    //    description: "Enter mobile number and user type to reset the password",
    //    returns: {
    //        arg: 'phoneUser',
    //        type: 'array'
    //    },
    //    accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}}],
    //    http: {
    //        path: '/userExists',
    //        verb: 'get'
    //    }
    //});

};
