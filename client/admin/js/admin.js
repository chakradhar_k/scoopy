/**
 * Created by User on 3/31/2016.
 */
var myApp = angular.module('myApp', ['ng-admin','restangular']);

myApp.config(['RestangularProvider', function (RestangularProvider) {
    RestangularProvider.addFullRequestInterceptor(function(element, operation, what, url, headers, params) {
        if (operation == "getList") {

            //console.log(url);
            // custom pagination params
            console.log(params);

            var queryString = {};

            if (params._page) {
                params.skip = (params._page - 1) * params._perPage;
                queryString['skip'] = params.skip;
                params.limit = params._perPage;
                queryString['limit'] = params.limit;
            }
            delete params.skip;
            delete params.limit;
            delete params._page;
            delete params._perPage;
            // custom sort params
            if (params._sortField) {
                //params._sort = params._sortField;
                //params._order = params._sortDir;
                //filterString = filterString + "{order:" + ":'" +params._sortField + " " + params._sortDir+"'}";
                queryString['order'] = params._sortField + ' ' + params._sortDir;
                delete params._sortField;
                delete params._sortDir;
            }
            // custom filters
            if (params._filters) {
                var filterString = {};
                for (var filter in params._filters) {
                    //console.log('ff: '+ JSON.stringify(filter));
                    var str = ".*"+params._filters[filter]+".*";
                    filterString[filter] = {like: params._filters[filter]+'.*', options: 'i' };
                    //params[filter] = params._filters[filter];
                }
                queryString['where'] = filterString;
                console.log(queryString);
                delete params._filters;
            }

            params['filter'] = queryString;

        }
        return { params: params };
    });

    RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
        var extractedData;

        extractedData = data.data;

        return extractedData;
    });

}]);

myApp.config(['NgAdminConfigurationProvider', function (NgAdminConfigurationProvider) {
    var nga = NgAdminConfigurationProvider;

    var customHeaderTemplate =
        '<div class="navbar-header">' +
        '<a class="navbar-brand" href="#" ng-click="appController.displayHome()">' +

        '</a>' +
        '</div>' +
        '<p class="navbar-text navbar-right">' +
        '<a href="https://github.com/marmelab/ng-admin/blob/master/examples/blog/config.js">' +
        '<a href="#" onclick="logout()">Logout</a>' +
        '</a>' +
        '</p>';

    var admin = nga.application('CremeVille Admin').baseApiUrl('http://localhost:4848/api/');
    admin.header(customHeaderTemplate);
    nga.menu().autoClose(true)
    admin.menu(nga.menu()
            .addChild(nga.menu().title('Category')
                .addChild(nga.menu().title('Category List').link('/categories/list')
            ))
            .addChild(nga.menu().title('Store')
                .addChild(nga.menu().title('Store List').link('/stores/list')
            ))
            .addChild(nga.menu().title('OrderRequests')
                .addChild(nga.menu().title('Order Requests').link('/orderrequests/list'))
                .addChild(nga.menu().title('Create Orders').link('/orderrequests/create'))
                .addChild(nga.menu().title('Transfer Items To Store'))
        )
            .addChild(nga.menu().title('Store Items').link('/stores/list')
//                        .addChild(nga.menu().title('Product List').link('/products/list'))
        )
            .addChild(nga.menu().title('Items').link('/items/list')
//                        .addChild(nga.menu().title('Product List').link('/products/list'))
        )
            .addChild(nga.menu().title('Brand').link('/brands/list')
//                        .addChild(nga.menu().title('Product List').link('/products/list'))
        )
            .addChild(nga.menu().title('Flavour').link('/flavours/list')
//                        .addChild(nga.menu().title('Product List').link('/products/list'))
        )
            .addChild(nga.menu().title('Store Addresses').link('/addresses/list')
//                        .addChild(nga.menu().title('Product List').link('/products/list'))
        )
            .addChild(nga.menu().title('Products').link('/products/list')
//                        .addChild(nga.menu().title('Product List').link('/products/list'))
        )
            .addChild(nga.menu().title('Coupons').link('/coupons/list')
//                        .addChild(nga.menu().title('Product List').link('/products/list'))
        )
    );

    var coupon = nga.entity('coupons');

    var fieldSet = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),
        nga.field('description'),
        nga.field('code'),
        nga.field('category'),
        nga.field('type'),
        nga.field('value', 'number').format('0,0.00')
            .template('<span ng-class="{ \'red\': value < 0 }"><ma-number-column field="::field" value="::entry.values[field.name()]"></ma-number-column></span>'),
        nga.field('unit'),
        nga.field('startTime', 'datetime'),
        nga.field('expiryTime', 'datetime'),
        nga.field('status', 'boolean')
            .choices([
                {value: true, label: 'enabled'},
                {value: false, label: 'disabled'}
            ]),
        nga.field('minimumAmount', 'number').format('0,0.00'),
        nga.field('maximumAmount', 'number').format('0,0.00'),
        nga.field('fixedValue', 'number').format('0,0.00'),
        nga.field('assignedFor'),
        nga.field('assignedType'),
        //nga.field('assignedType', 'choice'),
        /* .choices([
         { value: null, label: 'select choice' },
         { value: 'customer', label: 'customer' },
         { value: 'admin', label: 'admin' }
         ]),*/

        nga.field('noOfTimes', 'number').format('$0,0.00'),
        nga.field('noOfTimes','number').format('0,0.00'),
        //nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        // nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg'),
        nga.field('imageUrl','template').label('Web Image')
            .template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />'),
        nga.field('imageUrlM','template').label('Mobile Image')
            .template('<img src="{{ entry.values.imageUrlM }}" height="42" width="42" />'),

        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ];
    //coupon.listView().fields(fieldSet);
    coupon.listView().sortField('code').fields(fieldSet).filters([
        nga.field('code')
    ]);

    coupon.creationView().fields([
        //nga.field('id').editable(false),
        nga.field('name')
            .validation({ required: true, minlength: 3, maxlength: 100 }),
        nga.field('description'),
        nga.field('code'),
        nga.field('category'),
        nga.field('type'),
        nga.field('value', 'number').format('0,0.00'),
        nga.field('unit'),
        nga.field('startTime', 'datetime'),
        nga.field('expiryTime', 'datetime'),
        nga.field('status', 'boolean')
            .choices([
                { value: true, label: 'enabled' },
                { value: false, label: 'disabled' }
            ]),
        nga.field('minimumAmount', 'number').format('0,0.00'),
        nga.field('maximumAmount','number').format('0,0.00'),
        nga.field('fixedValue','number').format('0,0.00'),
        nga.field('assignedFor'),
        nga.field('assignedType'),
        //nga.field('assignedType', 'choice'),
        /* .choices([
         { value: null, label: 'select choice' },
         { value: 'customer', label: 'customer' },
         { value: 'admin', label: 'admin' }
         ]),*/
        nga.field('noOfTimes','number').format('0,0.00'),
        nga.field('webImage', 'file')
            .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
            .validation({ required: true }),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        nga.field('mobileImage', 'file')
            .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
            .validation({ required: true }),
        nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')]);
    coupon.editionView().fields(fieldSet);
    coupon.deletionView().fields(fieldSet);
    admin.addEntity(coupon);


    var category = nga.entity('categories');

    var fieldSet2 = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),
        //nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        //nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg'),
        nga.field('imageUrl','template').label('Web Image')
            .template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />'),
        nga.field('imageUrlM','template').label('Image')
            .template('<img src="{{ entry.values.imageUrlM }}" height="42" width="42" />'),

        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')

    ];
    /* category.listView().fields(fieldSet2).filters([
     /!*  nga.field('q')
     .label('Full-Text')
     .pinned(true),*!/
     nga.field('q')
     .label('')
     .pinned(true)
     .template('<div class="input-group"><input type="text" ng-model="value" placeholder="Search" class="form-control"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span></div>'),
     nga.field('name', 'reference')
     .targetEntity(category)
     .targetField(nga.field('name'))
     .label('Category')
     ]);*/
    category.listView().sortField('name').fields(fieldSet2).filters([
        nga.field('name')
    ]);
    category.creationView().fields([
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('webImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            })
            .validation({required: true}),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        nga.field('mobileImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            })
            .validation({required: true}),
        nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')
        /* nga.field('imageUrl','template').label('')
         .template('<img src="{{ entry.values.imageUrl }}" height="70" width="200" />')*/
    ]);
    category.editionView().fields([
        nga.field('name'),
        nga.field('webImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            })
            .validation({required: true}),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        nga.field('mobileImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            })
            .validation({required: true}),
        nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')

    ]);
    //category.readOnly();

    //category.listView.actions(['import']);
    admin.addEntity(category);




    var brand = nga.entity('brands');

    var fieldSet4 = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),
        nga.field('description'),

        //nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        //nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg'),
        nga.field('imageUrl','template').label('Web Image')
            .template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />'),
        nga.field('imageUrlM','template').label('Mobile Image')
            .template('<img src="{{ entry.values.imageUrlM }}" height="42" width="42" />'),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ];
    //brand.listView().fields(fieldSet4);
    brand.listView().sortField('name').fields(fieldSet4).filters([
        nga.field('name')
    ]);
    brand.creationView().fields([
        // nga.field('id').editable(false),
        nga.field('name'),
        nga.field('description'),
        nga.field('webImage', 'file')
            .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
            .validation({ required: true }),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        nga.field('mobileImage', 'file')
            .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
            .validation({ required: true }),
        nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')]);
    brand.editionView().fields([
        nga.field('name'),
        nga.field('description'),
        nga.field('webImage', 'file')
            .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
            .validation({ required: true }),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        nga.field('mobileImage', 'file')
            .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
            .validation({ required: true }),
        nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')]);
    admin.addEntity(brand);


    /* var catBrand = nga.entity('catBrands');

     var fieldSet5 = [
     nga.field('id')
     ];
     catBrand.listView().fields(fieldSet5);
     catBrand.creationView().fields(fieldSet5);
     catBrand.editionView().fields(fieldSet5);
     admin.addEntity(catBrand);


     var catFla = nga.entity('catFlas');

     var fieldSet6 = [
     nga.field('id')

     ];
     catFla.listView().fields(fieldSet6);
     catFla.creationView().fields(fieldSet6);
     catFla.editionView().fields(fieldSet6);
     admin.addEntity(catFla);

     var container = nga.entity('containers');

     var fieldSet7 = [
     nga.field('id')

     ];
     container.listView().fields(fieldSet7);
     container.creationView().fields(fieldSet7);
     container.editionView().fields(fieldSet7);
     admin.addEntity(container);*/

    var customer = nga.entity('customers');

    var fieldSet8 = [
        //nga.field('id').editable(false),
        nga.field('phone'),
        nga.field('otp'),
        nga.field('loyalty')
    ];
    customer.listView().fields(fieldSet8);
    customer.creationView().fields(fieldSet8);
    customer.editionView().fields(fieldSet8);
    admin.addEntity(customer);

    var discount = nga.entity('discounts');
    var fieldSet9 = [
        //nga.field('id').editable(false),
        nga.field('type'),
        nga.field('amount', 'number').format('$0,0.00'),
        /*var fieldSet9 = [
         nga.field('id').editable(false),
         nga.field('type'),
         nga.field('amount','number').format('0,0.00')
         */
    ];
    discount.listView().fields(fieldSet9);
    discount.creationView().fields(fieldSet9);
    discount.editionView().fields(fieldSet9);
    admin.addEntity(discount);

//        var emailN = nga.entity('emailNs');
//
//        var fieldSet10 = [
//            nga.field('id').editable(false),
//            nga.field('to'),
//            nga.field('from'),
//            nga.field('subject'),
//            nga.field('text'),
//            nga.field('html')
//
//        ];
//        emailN.listView().fields(fieldSet10);
//        emailN.creationView().fields(fieldSet10);
//        emailN.editionView().fields(fieldSet10);
//        admin.addEntity(emailN);

    var favourite = nga.entity('favourites');

    var fieldSet11 = [
        //nga.field('id').editable(false),
        nga.field('createdBy'),
        nga.field('favouriteType'),
        nga.field('favouriteItem'/*,'json'*/)

    ];
    favourite.listView().fields(fieldSet11);
    favourite.creationView().fields(fieldSet11);
    favourite.editionView().fields(fieldSet11);
    admin.addEntity(favourite);

    /* var flaBrand = nga.entity('flaBrands');

     var fieldSet12 = [
     nga.field('id')

     ];
     flaBrand.listView().fields(fieldSet12);
     flaBrand.creationView().fields(fieldSet12);
     flaBrand.editionView().fields(fieldSet12);
     admin.addEntity(flaBrand);*/

    var flavour = nga.entity('flavours');

    var fieldSet13 = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')

    ];
    //flavour.listView().fields(fieldSet13);
    flavour.listView().sortField('name').fields(fieldSet13).filters([
        nga.field('name')
    ]);
    flavour.creationView().fields(fieldSet13);
    flavour.editionView().fields(fieldSet13);
    admin.addEntity(flavour);



    /* var loyalty = nga.entity('loyalties');

     var fieldSet15 = [
     nga.field('id').editable(false),
     nga.field('totalPoints','number').format('0,0.00'),
     nga.field('points','number').format('0,0.00'),
     nga.field('amount','number').format('0,0.00'),
     nga.field('currency')

     ];
     loyalty.listView().fields(fieldSet15);
     loyalty.creationView().fields(fieldSet15);
     loyalty.editionView().fields(fieldSet15);
     admin.addEntity(loyalty);
     */


    var order = nga.entity('orders');

    var fieldSet16 = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),

    ];
    //order.listView().fields(fieldSet16);
    order.listView().sortField('name').fields(fieldSet16).filters([
        nga.field('name')
    ]);
    order.creationView().fields(fieldSet16);
    order.editionView().fields(fieldSet16);
    admin.addEntity(order);

//        var phoneUser = nga.entity('phoneUsers');
//
//        var fieldSet17 = [
//            nga.field('id').editable(false),
//            nga.field('phone')
//
//        ];
//        phoneUser.listView().fields(fieldSet17);
//        phoneUser.creationView().fields(fieldSet17);
//        phoneUser.editionView().fields(fieldSet17);
//        admin.addEntity(phoneUser);

    /* var price = nga.entity('prices');

     var fieldSet18 = [
     nga.field('id').editable(false),
     nga.field('basePrice','number').format('0,0.00'),
     nga.field('currency')

     ];
     price.listView().fields(fieldSet18);
     price.creationView().fields(fieldSet18);
     price.editionView().fields(fieldSet18);
     admin.addEntity(price);*/

    /* var review = nga.entity('reviews');

     var fieldSet19 = [
     nga.field('id').editable(false),
     nga.field('rating','number').format('0,0.00'),
     nga.field('ratingFor'),
     nga.field('createdBy'),
     nga.field('feedback'),
     nga.field('ratingType')

     ];
     review.listView().fields(fieldSet19);
     review.creationView().fields(fieldSet19);
     review.editionView().fields(fieldSet19);
     admin.addEntity(review);*/

    var product = nga.entity('products');

    var fieldSet24 = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),
        nga.field('price', 'number').format('0,0.00'),
        nga.field('categoryId', 'reference')
            .isDetailLink(true)
            .label('Category')
            .targetEntity(category)
            .targetField(nga.field('name')),
        nga.field('brandId', 'reference')
            .isDetailLink(false)
            .label('Brand')
            .targetEntity(brand)
            .targetField(nga.field('name')),
        nga.field('flavourId', 'reference')
            .isDetailLink(false)
            .label('Flavour')
            .targetEntity(flavour)
            .targetField(nga.field('name')),
        //nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        //nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg'),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')

    ];
    /* product.listView().fields(fieldSet24,[
     nga.field('Items', 'referenced_list')
     .targetEntity(nga.entity('items'))
     .targetReferenceField('productId')
     .targetFields([
     //nga.field('id'),
     nga.field('name').isDetailLink(true)
     ])
     ]);*/
    product.listView().sortField('name').fields(fieldSet24,[
        nga.field('Items', 'referenced_list')
            .targetEntity(nga.entity('items'))
            .targetReferenceField('productId')
            .targetFields([
                //nga.field('id'),
                nga.field('name').isDetailLink(true)
            ])
    ]).filters([
        nga.field('name'),
        nga.field('price'),
        nga.field('categoryId').label('Category'),
        nga.field('brandId').label('Brand'),
        nga.field('flavourId').label('Flavour')

    ]);
    product.creationView().fields(fieldSet24);




    var store = nga.entity('stores');

    var fieldSet20 = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),
        nga.field('email')
            .validation({required: true}),
        //nga.field('phone'),
        //nga.field('otpStatus','boolean'),
        //nga.field('otp')
        nga.field('geoLocation.lat').label('latitude'),
        nga.field('geoLocation.lng').label('longitude'),
        nga.field('Products', 'referenced_list')
            .targetEntity(nga.entity('products'))
            //.targetReferenceField('storeId')
            .targetFields([
                //nga.field('id'),
                nga.field('name'),
                nga.field('Items', 'referenced_list')
                    .targetEntity(nga.entity('items'))
                    .targetReferenceField('storeId')
                    .targetFields([
                        //nga.field('id'),
                        nga.field('name').isDetailLink(true)
                    ])
            ]),
        nga.field('Items', 'referenced_list')
            .targetEntity(nga.entity('items'))
            .targetReferenceField('storeId')
            .targetFields([
                //nga.field('id'),
                nga.field('name')
            ]),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
//            nga.field('password'),
        /*nga.field('address.street')
         .label('Street'),
         nga.field('address.city')
         .label('City'),
         nga.field('address.zipcode')
         .label('Zipcode')
         .validation({ pattern: '[A-Z\-0-9]{5,10}' }),*/
//            nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ];
    //store.listView().fields(fieldSet20);
    store.listView().sortField('name').fields(fieldSet20).filters([
        nga.field('name')
    ]);

    store.creationView().fields([
        nga.field('name').isDetailLink(true),
        nga.field('email')
            .validation({required: true}),
        //nga.field('phone'),
        //nga.field('otpStatus','boolean'),
        //nga.field('otp')
        nga.field('geoLocation.lat').label('latitude'),
        nga.field('geoLocation.lng').label('longitude'),
        nga.field('Products', 'referenced_list')
            .targetEntity(nga.entity('products'))
            //.targetReferenceField('storeId')
            .targetFields([
                //nga.field('id'),
                nga.field('name'),
                nga.field('Items', 'referenced_list')
                    .targetEntity(nga.entity('items'))
                    .targetReferenceField('storeId')
                    .targetFields([
                        //nga.field('id'),
                        nga.field('name').isDetailLink(true)
                    ])
            ]),
        nga.field('Items', 'referenced_list')
            .targetEntity(nga.entity('items'))
            .targetReferenceField('storeId')
            .targetFields([
                //nga.field('id'),
                nga.field('name')
            ])
    ]);
    store.editionView().fields(fieldSet20);
    admin.addEntity(store);


    var item = nga.entity('items');

    var fieldSet14 = [
        //nga.field('id').editable(false),
        nga.field('name').isDetailLink(true),
        nga.field('description'),
        nga.field('price', 'number').format('0,0.00'),
        nga.field('storeId', 'reference')
            .isDetailLink(false)
            .label('Store')
            .targetEntity(store)
            .targetField(nga.field('name')),

        nga.field('productId', 'reference')
            .isDetailLink(false)
            .label('Product')
            .targetEntity(product)
            .targetField(nga.field('name')),
        nga.field('categoryId', 'reference')
            .isDetailLink(false)
            .label('Category')
            .targetEntity(category)
            .targetField(nga.field('name')),
        nga.field('brandId', 'reference')
            .isDetailLink(false)
            .label('Brand')
            .targetEntity(brand)
            .targetField(nga.field('name')),
        nga.field('flavourId', 'reference')
            .isDetailLink(false)
            .label('Flavour')
            .targetEntity(flavour)
            .targetField(nga.field('name')),
        //nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        //nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg'),
        nga.field('imageUrl','template').label('Web Image')
            .template('<img src="{{ entry.values.imageUrl }}" height="42" width="42" />'),
        nga.field('imageUrlM','template').label('Mobile Image')
            .template('<img src="{{ entry.values.imageUrlM }}" height="42" width="42" />'),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')

    ];
    //item.listView().fields(fieldSet14);
    item.listView().sortField('name').fields(fieldSet14).filters([
        nga.field('name'),
        nga.field('price')
    ]);
    //item.creationView().fields(fieldSet14, [
    item.creationView().fields([
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('price','number').format('0,0.00'),
        nga.field('description'),
        nga.field('productId', 'reference')
            .isDetailLink(false)
            .label('Product')
            .targetEntity(product)
            .targetField(nga.field('name')),
        nga.field('categoryId', 'reference')
            .isDetailLink(false)
            .label('Category')
            .targetEntity(category)
            .targetField(nga.field('name')),
        /*nga.field('storeId', 'reference')
         .isDetailLink(false)
         .label('Store')
         .targetEntity(store)
         .targetField(nga.field('name')),
         */            nga.field('brandId', 'reference')
            .isDetailLink(false)
            .label('Brand')
            .targetEntity(brand)
            .targetField(nga.field('name')),
        nga.field('flavourId', 'reference')
            .isDetailLink(false)
            .label('Flavour')
            .targetEntity(flavour)
            .targetField(nga.field('name')),
        nga.field('webImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            })
            .validation({required: true}),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        nga.field('mobileImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            }),

        nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')]);
    item.editionView().fields([
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('price','number').format('0,0.00'),
        nga.field('description'),
        nga.field('productId', 'reference')
            .isDetailLink(false)
            .label('Product')
            .targetEntity(product)
            .targetField(nga.field('name')),
        nga.field('categoryId', 'reference')
            .isDetailLink(false)
            .label('Category')
            .targetEntity(category)
            .targetField(nga.field('name')),
        nga.field('storeId', 'reference')
            .isDetailLink(false)
            .label('Store')
            .targetEntity(store)
            .targetField(nga.field('name')),
        nga.field('brandId', 'reference')
            .isDetailLink(false)
            .label('Brand')
            .targetEntity(brand)
            .targetField(nga.field('name')),
        nga.field('flavourId', 'reference')
            .isDetailLink(false)
            .label('Flavour')
            .targetEntity(flavour)
            .targetField(nga.field('name')),
        nga.field('webImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            })
            .validation({required: true}),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
        nga.field('mobileImage', 'file')
            .uploadInformation({
                'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload',
                'apifilename': 'picture_name',
                'upload': 'image',
                'accept': 'image*'
            }),

        nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')]);
    admin.addEntity(item);

    var tax = nga.entity('taxes');

    var fieldSet21 = [
        //nga.field('id').editable(false),
        nga.field('vatTax'),
        nga.field('deliveryCharge'),
        nga.field('minimumAmount', 'number').format('0'),
        nga.field('minimumAmount','number').format('0,0.00')
    ];
    //tax.listView().fields(fieldSet21);
    tax.listView().sortField('name').fields(fieldSet21).filters([
        nga.field('vatTax')
    ]);
    tax.creationView().fields(fieldSet21);
    tax.editionView().fields(fieldSet21);
    admin.addEntity(tax);
//        product.creationView().fields(fieldSet24,[
//            nga.field('webImage', 'file')
//                    .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
//                    .validation({ required: true }),
//            nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/web.jpg'),
//            nga.field('mobileImage', 'file')
//                    .uploadInformation({ 'url': 'http://104.196.99.177:4848/api/Containers/scoopy/upload', 'apifilename': 'picture_name','upload': 'image', 'accept': 'image*' })
//                    .validation({ required: true }),
//            nga.field('imageUrlM').defaultValue('https://s3-us-west-2.amazonaws.com/scoopy/mobile.jpg')]);
    product.editionView().fields(fieldSet24);
    admin.addEntity(product);


    var address = nga.entity('addresses');

    var fieldSet3 = [
        nga.field('id').editable(false),
        nga.field('type'),
        nga.field('zipCode'),
        nga.field('streetName'),
        nga.field('streetAddress'),
        nga.field('secondaryAddress'),
        nga.field('city'),
        nga.field('state'),
        nga.field('country'),
        nga.field('storeId', 'reference')
            .isDetailLink(false)
            .label('Store')
            .targetEntity(store)
            .targetField(nga.field('name')),
        nga.field('geoLocation.lat').label('latitude'),
        nga.field('geoLocation.lng').label('longitude'),
    ];
    //address.listView().fields(fieldSet3);
    address.listView().sortField('name').fields(fieldSet3).filters([
        nga.field('zipCode'),
        nga.field('city'),
        nga.field('state'),
        nga.field('type')
    ]);
    address.creationView().fields(fieldSet3);
    address.editionView().fields(fieldSet3);
    admin.addEntity(address);


    var orderrequest = nga.entity('orderrequests');

    var opField = true;

    var operationChange = function(operation) {

        opField = operation;

    }

    var fieldSet22 = [
        //nga.field('id').editable(false),
        nga.field('operation').defaultValue('create').editable(false),
        nga.field('type').defaultValue('product').editable(false),
        nga.field('quantity', 'number').format('0,0.00'),
        nga.field('product', 'reference')
            .isDetailLink(true)
            .label('Product')
            .targetEntity(product)
            .targetField(nga.field('name')),
        nga.field('store', 'reference')
            .attributes({ 'ng-show': "entry.values.operation != 'create'" })
            .isDetailLink(true)
            .label('store')
            .targetEntity(store)
            .targetField(nga.field('name')),
        nga.field('toStore', 'reference')
            .attributes({ 'ng-show': "opField" })
            .isDetailLink(true)
            .label('Transfer To Store')
            .targetEntity(store)
            .targetField(nga.field('name')),
        nga.field('status', 'boolean')
    ];
    //orderrequest.listView().fields(fieldSet22);
    orderrequest.listView().sortField('name').fields(fieldSet22).filters([
        nga.field('quantity'),
        nga.field('store')
    ]);
    orderrequest.creationView().fields(fieldSet22);
    orderrequest.editionView().fields(fieldSet22);
    admin.addEntity(orderrequest);

    /*var orderrequest1 = nga.entity('orderrequests');

     var fieldSet29 = [
     nga.field('id').editable(false),
     nga.field('operation').defaultValue('create').editable(false),
     nga.field('type').defaultValue('product').editable(false),
     nga.field('quantity', 'number').format('$0,0.00'),
     nga.field('product', 'reference')
     .isDetailLink(true)
     .label('Product')
     .targetEntity(product)
     .targetField(nga.field('name')),
     nga.field('fromStore', 'reference')
     .isDetailLink(true)
     .label('Transfer From Store')
     .targetEntity(store)
     .targetField(nga.field('name')),
     nga.field('toStore', 'reference')
     .isDetailLink(true)
     .label('Transfer To Store')
     .targetEntity(store)
     .targetField(nga.field('name')),
     nga.field('status', 'boolean')
     ];
     orderrequest1.listView().fields(fieldSet29);
     orderrequest1.creationView().fields(fieldSet29);
     orderrequest1.editionView().fields(fieldSet29);
     admin.addEntity(orderrequest1);
     */
    nga.configure(admin);

}]);

myApp.config(['RestangularProvider', function (RestangularProvider) {
    RestangularProvider.addFullRequestInterceptor(function (element, operation, what, url, headers, params) {
        if (operation == "getList") {
            // custom pagination params
            if (params._page) {
                params._start = (params._page - 1) * params._perPage;
                params._end = params._page * params._perPage;
            }
            delete params._page;
            delete params._perPage;
            // custom sort params
            if (params._sortField) {
                params._sort = params._sortField;
                params._order = params._sortDir;
                delete params._sortField;
                delete params._sortDir;
            }
            // custom filters
            if (params._filters) {
                for (var filter in params._filters) {
                    params[filter] = params._filters[filter];
                }
                delete params._filters;
            }
        }
        return {params: params};
    });
}]);
