
var app = angular.module('myApp',[]);

app.controller('payUCallBack', function ($scope,$http,$window,$location){
    console.log("Pay u controller");

    $window.localStorage.removeItem('cartList');
    $window.localStorage.removeItem('cartTotalAmount');
    $window.localStorage.removeItem("cartCount");

    $scope.updateOrder = function( txnId, orderId, phone, name, amount, email){
        console.log("Pay u controller...........");
        $http({
            "method":"PUT",
            "url": 'api/Orders/'+orderId,
            "headers": {'Content-Type': 'application/json','Accept': 'application/json'},
            "data":{
                "paymentStatus": true,
                "transactionId": txnId,
                "paymentMode": "PAYUMONEY"
            }
        }).success(function(response){
            $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));
            var accessToken = $window.localStorage.getItem('accessToken');
           console.log("After Payment:"+JSON.stringify(response));
            $http({
                method: "GET",
                url: 'api/Customers?filter={"where":{"phone":"'+phone+'"}}',
                headers: {'Accept': 'application/json'}
            }).success(function(data){
                console.log("User Info after order:"+JSON.stringify(data.data));
                $window.localStorage.setItem('userInfo', JSON.stringify(data.data));
            }).error(function(data){
                console.log("....................."+JSON.stringify(data));
            });
            $window.location.href="http://"+$window.location.host+'/#/orderSummery';
        });
    };
});

console.log('how are you?');