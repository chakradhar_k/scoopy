 jQuery(document).ready(function(){
    $('.qtyplus').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal)) {
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            $('input[name='+fieldName+']').val(0);
        }
    });
    $(".qtyminus").click(function(e) {
        e.preventDefault();
        fieldName = $(this).attr('field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal) && currentVal > 0) {
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {
            $('input[name='+fieldName+']').val(0);
        }
    });
});
$(document).click( function(){
	$(this).removeClass('cd-active');
});
$(document).ready(function(){ 
    $("#myTab li:eq(1) a").tab('show');
    $("#myTab1 li:eq(1) a").tab('show');
	
	
	
	$('.country').on('change', function (e) {

		var optionSelected = $("option:selected", this);
		var valueSelected = this.value;
		if (valueSelected == "Canada") {
			$('.pro').html('<option value ="BC">British Columbia</option><option value="Ontario">Ontario</option>');
			// remove the city <select> dropdown
		} else if (valueSelected == "United_States") {
			$('.pro ').html('<option value ="FL">Florida</option><option value="NY">New York</option>');
		}
	});
	$('.pro').on('change', function (e) {
		var optionSelectedb = $("option:selected", this);
		var valueSelectedb = this.value;
		console.log(valueSelectedb);
		$(".city option").remove();
		if (valueSelectedb == "FL") {
			$(".city").append("<option value=\"Miami\">Miami</option><option value=\"Orlando\">Orlando</option>");
		} else if (valueSelectedb == "NY") {
			$(".city").append("<option value=\"New York\">New York</option><option value=\"Buffalo\">Buffalo</option></select>");
		}
	});
	
});

/* Float Label Pattern Plugin for Bootstrap 3.1.0 by Travis Wilson
**************************************************/

(function ($) {
    $.fn.floatLabels = function (options) {

        // Settings
        var self = this;
        var settings = $.extend({}, options);


        // Event Handlers
        function registerEventHandlers() {
            self.on('input keyup change', 'input, textarea', function () {
                actions.swapLabels(this);
            });
        }


        // Actions
        var actions = {
            initialize: function() {
                self.each(function () {
                    var $this = $(this);
                    var $label = $this.children('label');
                    var $field = $this.find('input,textarea').first();

                    if ($this.children().first().is('label')) {
                        $this.children().first().remove();
                        $this.append($label);
                    }

                    var placeholderText = ($field.attr('placeholder') && $field.attr('placeholder') != $label.text()) ? $field.attr('placeholder') : $label.text();

                    $label.data('placeholder-text', placeholderText);
                    $label.data('original-text', $label.text());

                    if ($field.val() == '') {
                        $field.addClass('empty')
                    }
                });
            },
            swapLabels: function (field) {
                var $field = $(field);
                var $label = $(field).siblings('label').first();
                var isEmpty = Boolean($field.val());

                if (isEmpty) {
                    $field.removeClass('empty');
                    $label.text($label.data('original-text'));
                }
                else {
                    $field.addClass('empty');
                    $label.text($label.data('placeholder-text'));
                }
            }
        }


        // Initialization
        function init() {
            registerEventHandlers();

            actions.initialize();
            self.each(function () {
                actions.swapLabels($(this).find('input,textarea').first());
            });
        }
        init();


        return this;
    };

    $(function () {
        $('.float-label-control').floatLabels();
    });
})(jQuery);

 //$(document).ready(function(){
     /*var $animation_elements = $('.animation-element');
     var $window = $(window);


     function check_if_in_view() {
         var window_height = $window.height();
         var window_top_position = $window.scrollTop();
         var window_bottom_position = (window_top_position + window_height);

         $.each($animation_elements, function() {
             var $element = $(this);
             var element_height = $element.outerHeight();
             var element_top_position = $element.offset().top;
             var element_bottom_position = (element_top_position + element_height);

             //check to see if this current container is within viewport
             if ((element_bottom_position >= window_top_position) &&
                 (element_top_position <= window_bottom_position)) {
                 $element.addClass('in-view');
                 alert('add class');
             } else {
                 $element.removeClass('in-view');
                 alert('remove class');
             }
         });
     }

     $window.on('scroll resize', check_if_in_view);
     $window.trigger('scroll');*/

     $('#us3').locationpicker({
     location: {latitude: 46.15242437752303, longitude: 2.7470703125},
     radius: 300,
     inputBinding: {
         latitudeInput: $('#us3-lat'),
         longitudeInput: $('#us3-lon'),
         radiusInput: $('#us3-radius'),
         locationNameInput: $('#us3-address')
     },
     enableAutocomplete: true
 });
 $('#selectlocation').on('shown.bs.modal', function() {
     $('#us3').locationpicker('autosize');

 });