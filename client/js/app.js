var cart=[];

var app = angular.module('myApp', ['ngRoute','ngAnimate', 'ui.bootstrap']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/html/home.html',
            controller: 'homeController'
        }).when('/login', {
            templateUrl: '/html/login-with-otp.html',
            controller: 'loginController'
        }).when('/category/:id', {
            templateUrl: '/html/product_catalogue.html',
            controller: 'productsController'
        }).when('/cart-items', {
            templateUrl: '/html/cart-item.html',
            controller: 'cartController'
        }).when('/addAddress', {
            templateUrl: '/html/add-address.html',
            controller: 'addressController'
        }).when('/orderSummery', {
            templateUrl: '/html/order-summery.html',
            controller: 'orderController'
        }).when('/payUCallBack', {
            templateUrl:'/html/payUCallBack.html',
            controller: 'payUCallBack'
        })
        .when('/orderConfirmation',{
            templateUrl: '/html/order-confirmation.html',
            controller: 'orderController'
        }).when('/userProfileTabs',{
            templateUrl: '/html/user-profile-tabs.html',
            controller: 'userProfileController'
        }).when('/faq',{
            templateUrl: '/html/faq.html'
        }).when('/termsOfUse',{
            templateUrl: '/html/terms-of-use.html'
        }).when('/privacyPolicy',{
            templateUrl: '/html/privacy-policy.html'
        }).otherwise({
            redirectTo: '/'
        });
});

app.controller('indexController', function ($http, $scope,$window,$location, $rootScope) {
    console.log("Index Controller Entered");

    if($window.localStorage.getItem('currentLatitude') && $window.localStorage.getItem('currentLongitude')){
        $('#selectlocation').modal('hide');
        $('#errorLocation').modal('hide');
    }else{
        $('#selectlocation').modal('show');
    }
    $rootScope.errorMessage = false;
    $scope.initialize = function($rootScope,$scope) {
        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('txtAutocomplete'));
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            console.log("place details:"+JSON.stringify(place));
            $window.localStorage.setItem('currentLatitude',place.geometry.location.lat());
            $window.localStorage.setItem('currentLongitude',place.geometry.location.lng());
            $window.localStorage.setItem('currentAddress',place.formatted_address);
            $window.localStorage.removeItem('storeId');
            $http({
                method: "GET",
                url: 'api/Stores/getNearestStoreInformation?lat='+Number(place.geometry.location.lat())+'&lng='+Number(place.geometry.location.lng()),
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).success(function(data){
                console.log("data..."+JSON.stringify(data.data.stores));
                if(data.data.stores.length>0){
                    $window.localStorage.setItem('storeId',data.data.stores[0].id);
                    $('#selectlocation').modal('hide');
                }else{
                    $('#selectlocation').modal('hide');
                    $('#errorLocation').modal('show');
                    $rootScope.errorMessage = true;
                    $rootScope.messageLocation = "";
                }
            }).error(function(reponse){
                console.log("Location Error:"+JSON.stringify(reponse));
                if(reponse.message == "no store is available"){
                    $('#selectlocation').modal('hide');
                    $('#errorLocation').modal('show');
                    $rootScope.errorMessage = true;
                    $rootScope.messageLocation = "";
                }
            });
            console.log("location:"+location);
        });
    };
    google.maps.event.addDomListener(window, 'load', $scope.initialize());

    $rootScope.errorMessage = false;
    console.log("Locate my address..."+JSON.stringify($rootScope.enteredLocation));

    $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));
    if($scope.userInfo){
        $rootScope.showLoginHead = false;
        $rootScope.userName = $scope.userInfo.name;
    }else{
        $rootScope.showLoginHead = true;
    }

    if($window.localStorage.getItem('deliveryCharge')){
        $rootScope.deliveryCharges = Number($window.localStorage.getItem('deliveryCharge'));
        console.log("...exist..."+$rootScope.deliveryCharges);
        $rootScope.showFreeDelivery = false;
        if($rootScope.deliveryCharges == 0){
            $rootScope.showFreeDelivery = true;
        }
    }else{
        console.log("..Not...exist...");
        $rootScope.deliveryCharges = $rootScope.deliveryChargeAmount;
    }

    $http({
        method: "GET",
        url: 'api/Taxes',
        headers: {'Accept': 'application/json'}
    }).success(function(data){
       console.log("Tax Data:"+JSON.stringify(data.data[0]));
        $window.localStorage.setItem('tax',data.data[0].vatTax);
        $window.localStorage.setItem('deliveryChargeAmount',data.data[0].deliveryCharge);
        $window.localStorage.setItem('minimumAmount',data.data[0].minimumAmount);
        $rootScope.taxAmount = Number(data.data[0].vatTax);
        $rootScope.deliveryChargeAmount = Number(data.data[0].deliveryCharge);
        $rootScope.minimunAmount = Number(data.data[0].minimumAmount);
    });

    $rootScope.logout = function(){
        $window.localStorage.removeItem("userInfo");
        $window.localStorage.removeItem('storeId');
        $window.localStorage.removeItem('currentLatitude');
        $window.localStorage.removeItem('currentLongitude');
        $window.localStorage.removeItem('accessToken');
        console.log("Logout");
        $window.location.reload();
        $location.url('/');
    };

    //console.log("Position..."+$window.localStorage.getItem('currentLatitude'));

    $scope.currentLocation = function(){
        console.log("Current Location:");
        if (navigator.geolocation) {
            console.log('Geolocation is supported!');
            navigator.geolocation.getCurrentPosition(
                function success(position){
                    console.log("Position Info:"+JSON.stringify(position.coords));
                    console.log("Position:"+position.coords.latitude);
                    console.log("Position:lng:"+position.coords.longitude);
                    $window.localStorage.setItem('currentLatitude',position.coords.latitude);
                    $window.localStorage.setItem('currentLongitude',position.coords.longitude);
                    $window.localStorage.removeItem('storeId');

                    var lat = parseFloat(position.coords.latitude);
                    var lng = parseFloat(position.coords.longitude);
                    var latlng = new google.maps.LatLng(lat, lng);
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            console.log("Location Address:"+JSON.stringify(results[0].formatted_address));
                        }
                        $window.localStorage.setItem('currentAddress',results[0].formatted_address);
                    });

                        $http({
                            method: "GET",
                            url: 'api/Stores/getNearestStoreInformation?lat='+position.coords.latitude+'&lng='+position.coords.longitude,
                            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
                        }).success(function(data){
                            console.log("data..."+JSON.stringify(data.data.stores));
                            if(data.data.stores.length>0){
                                $window.localStorage.setItem('storeId',data.data.stores[0].id);
                                $('#selectlocation').modal('hide');
                            }else{
                                $('#selectlocation').modal('hide');
                                $('#errorLocation').modal('show');
                                $rootScope.errorMessage = true;
                                $rootScope.messageLocation = "";
                            }
                        }).error(function(reponse){
                            console.log("Location Error:"+JSON.stringify(reponse));
                            if(reponse.message == "no store is available"){
                                $('#selectlocation').modal('hide');
                                $('#errorLocation').modal('show');
                                $rootScope.errorMessage = true;
                                $rootScope.messageLocation = "";
                            }
                        });
                },
                function error(position){
                    console.log("Unable To Find Your Location. Please Check Your Settings or Enter Manually");
                });
        }
        else {
            console.log('Geolocation is not supported for this Browser/OS version yet.');
        }

    };

});

app.controller('loginController', function ($http, $scope,$window,$location, $rootScope) {
    console.log("Login Controller");
    //$scope.showLogin = false;
    console.log("Back Url:"+$window.history.href);
    $scope.user = {
        "password":"",
        "name":"",
        "phone":""
    };
    $scope.showOtpForm = false;
    $scope.getOtp = function(){
        console.log("Phone:"+JSON.stringify($scope.user));
        $http({
            method: "GET",
            url: 'api/Customers/userExists?phone='+$scope.user.phone,
            headers: {'Accept': 'application/json'}
        }).success(function (response) {
            console.log("Response Get OTP:"+JSON.stringify(response.data));
            $scope.user.name = response.data.customer.name;
            console.log("User name:"+$scope.user.name);
            $scope.newUser = $scope.user.name;
            $scope.showOtpForm = true;
            $('#otpSend').modal('show');
        });
    };

    $scope.confirmOtp = function(){
        console.log("Verify Otp:"+JSON.stringify($scope.user));
        $http({
            method: "POST",
            url: 'api/Customers/login',
            headers: {'Accept': 'application/json'},
            data: $scope.user
        }).success(function (response) {
            console.log("Response.. Confirm Otp..:"+JSON.stringify(response.data));
            if($scope.newUser){
                $window.localStorage.setItem('accessToken',response.data.id);
                $window.localStorage.setItem('userInfo',JSON.stringify(response.data.customer));
                $window.localStorage.setItem('accessToken',response.data.id);

                var reUrl = $window.localStorage.getItem("redirectUrl");
                console.log("REDIRECT URL " + reUrl);
                if (reUrl != undefined) {
                    $window.localStorage.removeItem("redirectUrl");
                    $window.location.reload();
                    window.location = reUrl;
                } else {
                    $window.location.reload();
                    $location.url('/');
                }
            }else {
                $window.localStorage.setItem('accessToken',response.data.id);
                $http({
                    method: "PUT",
                    url: 'api/Customers/' + response.data.userId + '?access_token=' + response.data.id,
                    headers: {'Accept': 'application/json'},
                    data: $scope.user
                }).success(function (response) {
                    console.log("Data:" + JSON.stringify(response));
                    $window.localStorage.setItem('userInfo', JSON.stringify(response.data));

                    var reUrl = $window.localStorage.getItem("redirectUrl");
                    console.log("REDIRECT URL " + reUrl);
                    if (reUrl != undefined) {
                        $window.localStorage.removeItem("redirectUrl");
                        $window.location.reload();
                        window.location = reUrl;
                    } else {
                        $window.location.reload();
                        $location.url('/');
                    }

                });
            }

        }).error(function(response){
            console.log("Response Validate Error..."+JSON.stringify(response.data));
            $scope.otpErrorMessage = response.data.message;
            $scope.otpMatched = false;
        });
    };
});

app.controller('homeController', function ($http, $scope,$window,$location, $rootScope) {

    if($window.localStorage.getItem('currentLatitude') && $window.localStorage.getItem('currentLongitude')){
        $('#selectlocation').modal('hide');
    }
    console.log("Home Controller Entered");
    if($window.localStorage.getItem("cartList")) {
        $rootScope.cartCount = $window.localStorage.getItem("cartCount");
        console.log("Home Controller Entered:  Length:" + $rootScope.cartCount);
    }

    $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));
    if($scope.userInfo){
        $rootScope.showLoginHead = false;
        $rootScope.userName = $scope.userInfo.name;
    }else{
        $rootScope.showLoginHead = true;
    }

    $http({
        method: "GET",
        url: 'api/Categories',
        headers: {'Accept': 'application/json'}
    }).success(function (response) {
       console.log("Categories:"+JSON.stringify(response.data));
        $scope.categories = response.data;
    });

    $scope.setCategoryId = function(cataId){
        $window.localStorage.setItem('categoryId',cataId);
    };

});

app.controller('productsController', function ($http, $scope,$window,$location, $rootScope) {
    if($window.localStorage.getItem('currentLatitude') && $window.localStorage.getItem('currentLongitude')){
        $('#selectlocation').modal('hide');
    }

    //$rootScope.cartQuantity = 1;
    $scope.cataId = $window.localStorage.getItem('categoryId');
    $scope.selectedItem = '';

    $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));

    $scope.productsData = function(categoryId) {
        $scope.dataAvailable = true;
        //console.log("Category Id:"+categoryId);
        $window.localStorage.setItem('categoryId',categoryId);
        $scope.cataId = categoryId;

        var itemsUrl = '';
        if($scope.cataId == "56cee6e8c4b8f7197ced15d5"){
            itemsUrl = 'api/Items?filter={"where":{"brandId":"56cee53dc4b8f7197ced15a1"}}';
        }else{
            itemsUrl = 'api/Items?filter={"where":{"categoryId":"'+$scope.cataId+'"}}';
        }

        $http({
            method: "GET",
            url: itemsUrl,
            headers: {'Accept': 'application/json'}
        }).success(function (response) {
            //console.log("Products:" + JSON.stringify(response.data));
            $rootScope.products = response.data;

            if($rootScope.products == ''){
                $scope.dataAvailable = false;
            }else {
                $http({
                    method: "GET",
                    url: 'api/Categories/getFlavoursAndBrands?categoryId=' + $scope.cataId,
                    headers: {'Accept': 'application/json'}
                }).success(function (response) {
                    console.log("Response Flavours and Brands:" + JSON.stringify(response.data));
                    $scope.flavoursData = [];
                    $scope.brandsData = [];
                    $scope.flavoursData = response.data.category.flavours;
                    $scope.brandsData = response.data.category.brands;
                });
            }
        });
    };
    if($scope.cataId) {
        $scope.productsData($scope.cataId);
    }

    $scope.flavourFilterArray = [];
    $scope.brandFilterArray = [];
    $scope.minPrice = null;
    $scope.maxPrice = null;
    $scope.priceRange = function(min, max){

            $scope.minPrice = min;
            $scope.maxPrice = max;
        $scope.filterByFlavourAndBrand();
    };

    $scope.filterByFlavourAndBrand = function(value,id,type){
        console.log(value+"..."+id+"..."+type);
        $scope.dataAvailable = true;
        if(type == 'Flavour'){

            if(value == true){
                $scope.flavourFilterArray.push({flavourId: id});
                console.log("Flavours Array..."+JSON.stringify($scope.flavourFilterArray));
            }else if(value == false){
                $scope.flavourFilterArray = $scope.flavourFilterArray.filter(function( obj ) {
                    return obj.flavourId !== id;
                });
                console.log("remove Flavour..."+JSON.stringify($scope.flavourFilterArray));
            }

        }else if(type == 'Brand'){

            if(value == true){
                $scope.brandFilterArray.push({brandId: id});
                console.log("Flavours Array..."+JSON.stringify($scope.brandFilterArray));
            }else if(value == false){
                $scope.brandFilterArray = $scope.brandFilterArray.filter(function( obj ) {
                    return obj.brandId !== id;
                });
            }
        }

        var flavourArray = [];
        var brandsArray = [];
        if($scope.flavourFilterArray.length == 0){
            flavourArray = [""];
        }else{
            flavourArray = $scope.flavourFilterArray;
        }

        if($scope.brandFilterArray.length == 0){
            brandsArray = [""];
        }else{
            if($scope.cataId != "56cee6e8c4b8f7197ced15d5"){
                brandsArray = $scope.brandFilterArray;
            }else{
                brandsArray = [{"brandId":"56cee53dc4b8f7197ced15a1"}]
            }
        }

        var filterArray = [
            {
                "or": flavourArray
            },
            {
                "or": brandsArray
            }
        ];

        if($scope.cataId != "56cee6e8c4b8f7197ced15d5"){
            filterArray.push({"categoryId":$scope.cataId});
        }
        if($scope.minPrice){
            filterArray.push({"price":{"gte":$scope.minPrice}});
        }
        if($scope.maxPrice){
            filterArray.push({"price":{"lte":$scope.maxPrice}});
        }
        console.log("Filter Array..."+JSON.stringify(filterArray));

        $http({
            method: "GET",
            url: 'api/Items?filter={"where":{"and":'+JSON.stringify(filterArray)+'}}',
            headers: {'Accept': 'application/json'}
        }).success(function (response) {
            console.log("Response Filter..."+JSON.stringify(response));
            $rootScope.products = response.data;
            if($rootScope.products.length == 0){
                $scope.dataAvailable = false;
            }
        });

    };
    $scope.productDescription = function(productId){
        $rootScope.productReview = [];
        $rootScope.showReviewForm = false;
        $rootScope.reviewErrorMessage = false;
        $rootScope.reviewComment = '';
        $http({
            method: "GET",
            url: 'api/Items?filter={"where":{"id":"'+productId+'"}}',
            headers: {'Accept': 'application/json'}
        }).success(function (response) {
            //console.log("Products:" + JSON.stringify(response.data));
            $rootScope.productDescriptionData = response.data[0];
            console.log("Products:" + JSON.stringify($rootScope.productDescriptionData));
            $rootScope.reviewAvailable = true;
            $http({
                method: "GET",
                url: 'api/Reviews?filter={"where":{"ratingFor":"'+productId+'"}}',
                headers: {'Accept': 'application/json'}
            }).success(function(data){
               console.log("Review Data:"+JSON.stringify(data.data));
                $rootScope.productReview = data.data;
                if($rootScope.productReview.length == 0){
                    $rootScope.reviewAvailable = false;
                }
            });

            $http({
                method: "GET",
                url: 'api/Items?filter={"where":{"and":[{"categoryId":"'+$rootScope.productDescriptionData.categoryId+'"},{"id":{"neq":"'+$rootScope.productDescriptionData.id+'"}}]}}',
                headers: {'Accept': 'application/json'}
            }).success(function(data){
               //console.log("Same brand:"+JSON.stringify(data));
                $scope.sameBrandSuggestion = data.data;
            });
            $http({
                method: "GET",
                url: 'api/Items?filter={"where":{"and":[{"brandId":"56cee53dc4b8f7197ced15a1"},{"categoryId":"'+$rootScope.productDescriptionData.categoryId+'"},{"id":{"neq":"'+$rootScope.productDescriptionData.id+'"}}]}}',
                headers: {'Accept': 'application/json'}
            }).success(function (response) {
                console.log("Same Cremeville:"+JSON.stringify(response));
                $scope.cremevilleSuggestion = response.data;
            });
        });
    };
    $scope.filterByBrand = function(brand){
        console.log("Filter Brand:"+brand);
        $http({
            method: "GET",
            url: 'api/Items?filter={"where":{"and":[{"categoryId":"'+$scope.cataId+'"},{"brandName":"'+brandId+'"}]}}',
            headers: {'Accept': 'application/json'}
        }).success(function (response) {
            console.log("Same Category:"+JSON.stringify(response));
            $rootScope.products = response.data;
        });
    };

    $scope.cartItems = function(product,descrition){
        cart = [];

        if(descrition == 'descDialog'){
            var quantity = Number(document.getElementById('descDialog_'+product.id).value);
        }else {
            var quantity = Number(document.getElementById(product.id).value);
        }
        console.log("Cart Entered:"+quantity);

        if($window.localStorage.getItem("cartList")) {
            cart = JSON.parse($window.localStorage.getItem("cartList"));
            console.log("Cart .:"+JSON.stringify(cart));
        }

        for(var i=0; i< cart.length; i++){
            if(cart[i].id == product.id){
                cart.splice(i,1);
            }
        }
        cart.push({cartItem:product,id:product.id, quantity:quantity, total: (product.price * quantity)});
        //cart = _.uniq(cart, 'id');
        $rootScope.cartCount = cart.length;
        $window.localStorage.setItem("cartCount",$rootScope.cartCount);
        $window.localStorage.setItem("cartList",JSON.stringify(cart));
        console.log("Cart Entered:"+JSON.stringify(cart));
    };

    $scope.changeQuantity = function(changeQty,productId){
        var qty = Number(document.getElementById(productId).value);

        if(changeQty == 'decrease'){
            if(qty>1) {
                qty = qty - 1;
                document.getElementById(productId).value = qty;
                console.log("Qty change...-----" + qty);
            }
        }else if(changeQty == 'increase'){
            qty = qty + 1;
            document.getElementById(productId).value = qty;
            console.log("Qty change...+++++" + qty);
        }
    };

    $scope.addToFavourites = function(favProduct,type){
        console.log("Add to Favourites");
        if($scope.userInfo){
            console.log("User Favourites..."+JSON.stringify(favProduct));
            //$scope.userInfo.favourites = _.uniq($scope.userInfo.favourites, 'id');
            $http({
                method: "GET",
                url: 'api/Favourites?filter={"where":{"createdBy":"'+$scope.userInfo.id+'","favouriteItem.id":"'+favProduct.id+'"}}',
                headers: {'Accept': 'application/json'}
            }).success(function(response) {
                if(response.data.length == 0) {
                    $http({
                        method: "POST",
                        url: 'api/Favourites',
                        headers: {'Accept': 'application/json'},
                        data: {
                            "createdBy": $scope.userInfo.id,
                            "favouriteType": type,
                            "favouriteItem": {
                                "name": favProduct.name,
                                "id": favProduct.id,
                                "price": favProduct.price,
                                "imageUrl": favProduct.imageUrl
                            }
                        }
                    }).success(function (data) {
                        console.log("Response Update Favourite.." + JSON.stringify(data));
                        $rootScope.favouritesList = [];
                        $rootScope.favouritesList = data.data;
                    });
                } else{
                    console.log("Favourite Already Exist...");
                }
            });
        }
    };
});

app.controller('cartController', function ($http, $scope,$window,$location, $rootScope) {
    if($window.localStorage.getItem('currentLatitude') && $window.localStorage.getItem('currentLongitude')){
        $('#selectlocation').modal('hide');
    }

    $scope.storeError = false;
    $scope.cartTotal = function(){
        $rootScope.cartTotalAmount = 0;
        for(var i=0; i< $scope.cartList.length; i++){
            $rootScope.cartTotalAmount = $rootScope.cartTotalAmount + $scope.cartList[i].total;
        }
        console.log($rootScope.cartTotalAmount+'..........'+$rootScope.minimunAmount);
        if($rootScope.cartTotalAmount < 300){
            $rootScope.deliveryCharges = 50;
            $rootScope.showFreeDelivery = false;
        }else{
            $rootScope.deliveryCharges = 0;
            $rootScope.showFreeDelivery = true;
        }
        $window.localStorage.setItem('deliveryCharge',$rootScope.deliveryCharges);
        $window.localStorage.setItem('cartTotalAmount', $rootScope.cartTotalAmount);
        console.log("Total Amount:"+$rootScope.cartTotalAmount);
    };

    if($window.localStorage.getItem("cartList")) {
        $scope.cartItemsAvailable = true;
        $scope.cartList = JSON.parse($window.localStorage.getItem("cartList"));
        if($scope.cartList == ''){
            $scope.cartItemsAvailable = false;
        }
        console.log("Cart List.:"+JSON.stringify($scope.cartList));
        $scope.cartTotal();
    }
    $scope.errorMessage = false;
    $scope.changeQuantity = function(changeText,itemId){
        var quantity = Number(document.getElementById(itemId).value);
        if(changeText == 'decrease'){
            if(quantity>1) {
                quantity = quantity - 1;
                document.getElementById(itemId).value = quantity;
                console.log("Qty change...-----" + quantity);
            }
        }else if(changeText == 'increase'){
            quantity = quantity + 1;
            document.getElementById(itemId).value = quantity;
            console.log("Qty change...+++++" + quantity);
        }
        if(quantity>0) {
            $scope.errorMessage = false;
            console.log("Quantity:" + itemId + " ....." + quantity);
            for (var i in $scope.cartList) {
                if ($scope.cartList[i].id == itemId) {
                    $scope.cartList[i].quantity = quantity;
                    $scope.cartList[i].total = $scope.cartList[i].cartItem.price * quantity;
                    break; //Stop this loop, we found it!
                }
            }
            $scope.cartTotal();
            $window.localStorage.setItem("cartList", JSON.stringify($scope.cartList));
            console.log("Cart List.:" + JSON.stringify($scope.cartList));
            //$window.location.reload();
        }else{
            $scope.errorMessage = true;
            $scope.message = "Please give valid quantity";
        }
    };

    $scope.removeCartDetails = function(index, productName){
        $scope.cartIndex = index;
        $scope.removeProductName = productName;
        console.log("Remove Details:"+ index+ " ..."+productName);
    };

    $scope.removeCartItem = function(index){
        $scope.cartList.splice(index,1);
        console.log("Cart List After Remove.:"+JSON.stringify($scope.cartList));
        $window.localStorage.setItem("cartList",JSON.stringify($scope.cartList));
        $rootScope.cartCount = $scope.cartList.length;
        $window.localStorage.setItem("cartCount",$rootScope.cartCount);
        $scope.cartTotal();
        if($scope.cartList.length == 0){
            $window.location.reload();
        }
        $('#removeCart').modal('hide');
    };

    $scope.checkOut = function(){
        $scope.storeError = false;
        if($window.localStorage.getItem('storeId')) {
            if ($window.localStorage.getItem("userInfo")) {
                $window.localStorage.removeItem('previousUrl');
                $location.url('/orderConfirmation');
            } else {
                var url = $window.location.href;
                $window.localStorage.setItem("redirectUrl", url);
                $location.url('/login');
            }
        }else{
            $('#changeLocation').modal('show');
            $scope.storeError = true;
            $scope.storeErrorMessage = "We are not providing services at your location. Please change your location.";
        }
    };

   /* if($window.localStorage.getItem('changeLocation')){
        $scope.storeError = false;
        $('#changeLocation').modal('show');
        $window.localStorage.removeItem('locationChange');
    }*/
    $scope.changeYourLocation = function () {
        //$scope.storeError = false;
        $window.localStorage.removeItem('storeId');
        $window.localStorage.removeItem('currentLatitude');
        $window.localStorage.removeItem('currentLongitude');
        $window.location.reload();
    };
});

/*
app.controller('addressController', function ($http, $scope,$window,$location, $rootScope) {
    if($window.localStorage.getItem('currentLatitude') && $window.localStorage.getItem('currentLongitude')){
        $('#selectlocation').modal('hide');
    }

    $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));
    console.log("User Info:"+JSON.stringify($scope.userInfo));



    $scope.updateAddress = function(){
      console.log("User Info Address:"+JSON.stringify($scope.userInfo));
        var accessToken = $window.localStorage.getItem('accessToken');
        $http({
            method: "PUT",
            url: 'api/Customers/'+$scope.userInfo.id+'?access_token='+accessToken,
            headers: {'Accept': 'application/json'},
            data: $scope.userInfo
        }).success(function (response) {
            console.log("Response Address:"+JSON.stringify(response));
            $window.localStorage.setItem('userInfo',JSON.stringify(response.data));
           $location.url('/orderConfirmation');
        });
    };

});
*/

//USER_PROFILE_CONTROLLER
app.controller('userProfileController',function ($http,$scope,$window,$location,$rootScope){
    if($window.localStorage.getItem('currentLatitude') && $window.localStorage.getItem('currentLongitude')){
        $('#selectlocation').modal('hide');
    }

    $scope.oneAtATime = true;
    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
    $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));
    console.log("User Info : " + JSON.stringify($scope.userInfo));

    $scope.successMessage=false;
    $scope.errorAddressType = false;
    $scope.hideType = false;
    $scope.storeErrorAddress = true;
    //$rootScope.cartQuantity = 1;

    $scope.updateProfileInfo = function(){
        var accessToken = $window.localStorage.getItem("accessToken");
        $http({
            method: "PUT",
            url: 'api/Customers/'+$scope.userInfo.id+'?access_token='+accessToken,
            headers: {'Accept': 'application/json',
                      'Content-Type' : 'application/json'
                     },
            data: $scope.userInfo
        }).success(function(response) {
            console.log("Response USER INFO : " + JSON.stringify(response));
            $window.localStorage.setItem('userInfo', JSON.stringify(response.data));
            $('#editProfile').modal('hide');
            $location.url('/userProfileTabs');
            $scope.message = "Your Details Saved Successfully";
            $scope.successMessage=true;
        });
    };
    $scope.favouritesAvailable = true;

    $http({
        method: "GET",
        url: 'api/Favourites?filter={"where":{"createdBy":"'+$scope.userInfo.id+'"}}',
        headers: {'Accept': 'application/json'}
    }).success(function(response){
        $rootScope.favouritesList = [];
        $rootScope.favouritesList = response.data;
        console.log("Fav..."+JSON.stringify($rootScope.favouritesList));
        if($rootScope.favouritesList == ''){
            $scope.favouritesAvailable = false;
        }
    });

    var accessToken = $window.localStorage.getItem('accessToken');

    $scope.removeFavouriteDetails = function(favId){
            $scope.favId = favId;
    };

    $scope.removeFavourite = function(){

        $http({
            method: "DELETE",
            url: 'api/Favourites/' + $scope.favId,
            headers: {'Accept': 'application/json'},
        }).success(function(data){
            console.log("Response Update Favourite.."+JSON.stringify(data));
           //$window.location.reload();
            $http({
                method: "GET",
                url: 'api/Favourites?filter={"where":{"createdBy":"'+$scope.userInfo.id+'"}}',
                headers: {'Accept': 'application/json'}
            }).success(function(response){
                $rootScope.favouritesList = [];
                $rootScope.favouritesList = response.data;
                console.log("Fav..."+JSON.stringify($rootScope.favouritesList));
                if($rootScope.favouritesList == ''){
                    $scope.favouritesAvailable = false;
                }
                $('#removeFavourite').modal('hide');
            });
        });
    };

    $scope.changeYourLocation = function () {
        //$scope.storeError = false;
        $window.localStorage.removeItem('storeId');
        $window.localStorage.removeItem('currentLatitude');
        $window.localStorage.removeItem('currentLongitude');
        $window.location.reload();
    };

    $http({
        method: "GET",
        url: 'api/Addresses?filter={"where":{"customerId":"'+$scope.userInfo.id+'"}}',
        headers: {'Accept': 'application/json'}
    }).success(function(data){
        console.log("Address:"+JSON.stringify(data));
        $scope.addressList = data.data;
        if($scope.addressList.length < 3){
            $rootScope.showAddAddress = true;
        }else{
            $rootScope.showAddAddress = false;
        }
    });

    $scope.addAddressForm = function(type){
        console.log("dsfh......."+type)
        $scope.hideType = false;
        $scope.storeErrorAddress = true;
        $rootScope.newAddress = type;
        if($window.localStorage.getItem("storeId")) {
            $rootScope.addressToEdit = {
                areaName: $window.localStorage.getItem("currentAddress"),
                store_Id: $window.localStorage.getItem("storeId")
            };
            $('#editAddress').modal('show');
            console.log("StoreId:"+$window.localStorage.getItem("storeId"));
        }else{
            //alert("No Stores");
            $scope.storeErrorAddress = true;
            $('#changeLocation').modal('show');
        }
        //console.log("st address:"+$scope.addressToEdit.streetAddress);
    };

    $scope.editAddressBook = function(address){
        $scope.errorAddressType = false;
        $scope.hideType = true;
        console.log("Edit Address:"+JSON.stringify(address));
        $rootScope.addressToEdit = address;
        /*if($window.localStorage.getItem("currentAddress")) {
            $scope.addressToEdit.streetAddress = $window.localStorage.getItem("currentAddress");
        }*/
    };

    $scope.checkAddressType = function(addressType){
        console.log("AddType:"+addressType+'........UserId:'+$scope.userInfo.id);
        $http({
            method: "GET",
            url: 'api/Addresses?filter={"where":{"customerId":"'+$scope.userInfo.id+'","addressType":"'+addressType+'"}}',
            headers: {'Accept': 'application/json'}
        }).success(function(data){
            console.log("Address:"+JSON.stringify(data));
            if(data.data.length>0){
                $scope.errorAddressType = true;
                $scope.addressTypeError = 'This Address Type Already Exists, Please Select Another.';
            }else{
                $scope.errorAddressType = false;
            }
        });

    };

    $scope.updateAddress = function(){

        if($rootScope.addressToEdit.store_Id) {
            //$scope.addressToEdit.streetAddress = $window.localStorage.getItem("currentAddress");
            $scope.hideType = false;
            console.log("Update Address:" + JSON.stringify($rootScope.addressToEdit));
            console.log("New or Not:" + $rootScope.newAddress);
            console.log("Update Address:" + $rootScope.addressToEdit.addressType);
            if($rootScope.addressToEdit.addressType == null || $rootScope.addressToEdit.addressType == undefined){
                $scope.errorAddressType = true;
                $scope.addressTypeError = 'Please Select an Address Type to Continue.';
            }else {
                $scope.errorAddressType = false;
                if ($rootScope.newAddress == 'addNew') {
                    $rootScope.newAddress = '';
                    $http({
                        method: "POST",
                        url: 'api/Customers/' + $scope.userInfo.id + '/addresses?access_token=' + accessToken,
                        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                        data: $rootScope.addressToEdit
                    }).success(function (response) {
                        console.log("Response.New.:" + JSON.stringify(response.data));
                        /*if (response.data.length < 3) {
                         $rootScope.showAddAddress = true;
                         } else {
                         $rootScope.showAddAddress = false;
                         }*/
                        $http({
                            method: "GET",
                            url: 'api/Addresses?filter={"where":{"customerId":"' + $scope.userInfo.id + '"}}',
                            headers: {'Accept': 'application/json'}
                        }).success(function (data) {
                            console.log("Address:" + JSON.stringify(data));
                            $scope.addressList = data.data;
                            if ($scope.addressList.length < 3) {
                                $rootScope.showAddAddress = true;
                            } else {
                                $rootScope.showAddAddress = false;
                            }
                        });
                        //$window.location.reload();
                        $('#editAddress').modal('hide');
                    });
                } else {
                    $http({
                        method: "PUT",
                        url: 'api/Addresses/' + $rootScope.addressToEdit.id,
                        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                        data: $rootScope.addressToEdit
                    }).success(function (response) {
                        console.log("Response..:" + JSON.stringify(response.data));
                        $('#editAddress').modal('hide');
                    });
                }
            }
        }
    };

    $scope.removeAddressConfirm = function(addressId){
        $scope.removeAddressId = addressId;
    };

    $scope.removeAddress = function(addressId){
        $http({
            method: "DELETE",
            url: 'api/Addresses/'+addressId,
            headers: {'Accept': 'application/json'}
        }).success(function(data){
            console.log("Address:"+JSON.stringify(data));
            $('#removeAddress').modal('hide');
            $rootScope.showAddAddress = true;
            //$window.location.reload();
            $http({
                method: "GET",
                url: 'api/Addresses?filter={"where":{"customerId":"'+$scope.userInfo.id+'"}}',
                headers: {'Accept': 'application/json'}
            }).success(function(data){
                console.log("Address:"+JSON.stringify(data));
                $scope.addressList = data.data;
                if($scope.addressList.length < 3){
                    $rootScope.showAddAddress = true;
                }else{
                    $rootScope.showAddAddress = false;
                }
            });
        });
    };

    $scope.ordersAvailable = true;
    $http({
        method: "GET",
        url: 'api/Orders?filter={"where":{"customerId":"'+$scope.userInfo.id+'"}}',
        headers: {'Accept': 'application/json'}
    }).success(function(response){
        $scope.myOrderList = response.data;
        if($scope.myOrderList == ''){
            $scope.ordersAvailable = false;
        }
    });

    $http({
        method: "GET",
        url: 'api/Coupons',
        headers: {'Accept': 'application/json'}
    }).success(function(data){
        console.log("Coupons Data:"+JSON.stringify(data));
        $scope.couponsList = data.data;
    });
});

//ORDER_CONTROLLER
app.controller('orderController', function ($http, $scope,$window,$location, $rootScope) {

    if($window.localStorage.getItem('currentLatitude') && $window.localStorage.getItem('currentLongitude')){
        $('#selectlocation').modal('hide');
    }

    if($window.localStorage.getItem('previousUrl') && $window.location.href != 'http://'+$window.location.host+'/#/orderSummery'){
        $location.url('/userProfileTabs');
    }
    $scope.addressValidation = '';
    $scope.addressSelection = true;
    $scope.errorAddressType = false;
    $scope.hideType = false;
    $scope.couponError = false;
    $scope.couponSuccess = false;

    $scope.nearStoreId = $window.localStorage.getItem("storeId");
    $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));
    $scope.cartList = JSON.parse($window.localStorage.getItem("cartList"));
    $scope.totalCartAmount = Number($window.localStorage.getItem('cartTotalAmount'));
    $rootScope.taxAmount = $window.localStorage.getItem('tax');
    $scope.orderAmount = $scope.totalCartAmount + ($scope.totalCartAmount*$rootScope.taxAmount) + $rootScope.deliveryCharges;


    if($window.localStorage.getItem('orderCartItems')){
        $rootScope.orderSummeryCartList = JSON.parse($window.localStorage.getItem('orderCartItems'));
        console.log('order Cart:'+JSON.stringify($rootScope.orderSummeryCartList));
        var addressId = $window.localStorage.getItem('orderAddressId');
        console.log('order Cart:'+JSON.stringify($rootScope.orderAddress));
        $http({
            method: "GET",
            url: 'api/Addresses/'+addressId,
            headers: {'Accept': 'application/json'}
        }).success(function(data){
            console.log("Address:"+JSON.stringify(data));
            $rootScope.orderAddress = data.data;
        });
    }

    $http({
        method: "GET",
        url: 'api/Addresses?filter={"where":{"customerId":"'+$scope.userInfo.id+'"}}',
        headers: {'Accept': 'application/json'}
    }).success(function(data){
       console.log("Address:"+JSON.stringify(data));
        $scope.addressList = data.data;
        if($scope.addressList.length < 3){
            $rootScope.showAddAddress = true;
        }else{
            $rootScope.showAddAddress = false;
        }
    });

    $scope.orderDetails = {
        "customerId": $scope.userInfo.id,
        "storeId":$scope.nearStoreId,
        "addressId": "" ,
        "address":"",
        "cartItems": [],
        "subTotal": $scope.totalCartAmount,
        "tax": ($scope.totalCartAmount*$rootScope.taxAmount),
        "deliveryCharges": $rootScope.deliveryCharges,
        "cartValue": $scope.orderAmount,
        "couponCode":"",
        "couponStatus":"0",
        "couponAmount":0,
        "loyaltyStatus":"0",
        "loyaltyAmount":0,
        "loyaltyPoint":0,
        "paymentMode": "",
        "orderOriginId": "W",
        "orderStatus":"NEW",
        "paymentStatus": false,
        "transactionId":""
    };

    if($scope.cartList) {
        for (var i = 0; i < $scope.cartList.length; i++) {
            $scope.orderDetails.cartItems.push({
                id: $scope.cartList[i].id,
                name: $scope.cartList[i].cartItem.name,
                image: $scope.cartList[i].cartItem.imageUrl,
                price: $scope.cartList[i].cartItem.price,
                quantity: $scope.cartList[i].quantity,
                total: $scope.cartList[i].total
            })
        }
    }

    $scope.orderAddressEdit = function(address){
        console.log("Address edit:"+JSON.stringify(address));
        $rootScope.editAddress = address;
        /*if($window.localStorage.getItem("currentAddress"))
            $scope.editAddress.streetAddress = $window.localStorage.getItem("currentAddress");*/
        $scope.hideType = true;
        $scope.errorAddressType = false;
    };

    $scope.addAddressForm = function(type){
        console.log("dsfh order......."+type);
        $rootScope.newAddress = type;
        $scope.hideType = false;
        if($window.localStorage.getItem("storeId")) {
            $rootScope.editAddress = {
                areaName: $window.localStorage.getItem("currentAddress"),
                store_Id: $window.localStorage.getItem("storeId")
            };
            $('#editAddress').modal('show');
        }else{
            alert("No Stores in this area");
        }
    };

    $scope.checkAddressType = function(addressType){
        console.log("AddType:"+addressType+'........UserId:'+$scope.userInfo.id);
        $http({
            method: "GET",
            url: 'api/Addresses?filter={"where":{"customerId":"'+$scope.userInfo.id+'","addressType":"'+addressType+'"}}',
            headers: {'Accept': 'application/json'}
        }).success(function(data){
            console.log("Address:"+JSON.stringify(data));
            if(data.data.length>0){
                $scope.errorAddressType = true;
                $scope.addressTypeError = 'This Address Type Already Exists, Please Select Another.';
            }else{
                $scope.errorAddressType = false;
            }
        });
    };

    var accessToken = $window.localStorage.getItem('accessToken');
    $scope.updateAddress = function(){
        if($rootScope.editAddress.store_Id) {

            //$scope.editAddress.streetAddress = $window.localStorage.getItem("currentAddress");
            $scope.hideType = false;
            console.log("Update Address:" + JSON.stringify($rootScope.editAddress));
            console.log("New or Not:" + $rootScope.newAddress);
            if($rootScope.editAddress.addressType == null || $rootScope.editAddress.addressType == undefined){
                $scope.errorAddressType = true;
                $scope.addressTypeError = 'Please Select an Address Type to Continue.';
            }else {
                $scope.errorAddressType = false;
                if ($rootScope.newAddress == 'addNew') {
                    $rootScope.newAddress = '';
                    console.log("Accesss Token:" + accessToken);
                    $http({
                        method: "POST",
                        url: 'api/Customers/' + $scope.userInfo.id + '/addresses?access_token=' + accessToken,
                        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                        data: $rootScope.editAddress
                    }).success(function (response) {
                        console.log("Response.New.:" + JSON.stringify(response.data));
                        //$window.location.reload();
                        $http({
                            method: "GET",
                            url: 'api/Addresses?filter={"where":{"customerId":"' + $scope.userInfo.id + '"}}',
                            headers: {'Accept': 'application/json'}
                        }).success(function (data) {
                            console.log("Address:" + JSON.stringify(data));
                            $scope.addressList = data.data;
                            if ($scope.addressList.length < 3) {
                                $rootScope.showAddAddress = true;
                            } else {
                                $rootScope.showAddAddress = false;
                            }
                        });
                        $('#editAddress').modal('hide');
                    });
                } else {
                    $http({
                        method: "PUT",
                        url: 'api/Addresses/' + $rootScope.editAddress.id,
                        headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                        data: $rootScope.editAddress
                    }).success(function (response) {
                        console.log("Response..:" + JSON.stringify(response.data));
                        $('#editAddress').modal('hide');
                    });
                }
            }
        }else{
            alert("No Stores for this Address");
        }
    };

    $scope.selectedAddress = function(address){
        $scope.addressSelection = true;
        console.log("Selected:"+JSON.stringify(address));
        $scope.orderDetails.addressId = address.id;
        $scope.orderDetails.address = "H.No:"+address.houseNumber+","+address.landMark+","+address.streetAddress+","+address.city+"-"+address.pincode;
        $rootScope.orderAddress = address;
        $window.localStorage.setItem('orderAddressId', address.id);
        $scope.addressValidation = 'true';
        console.log("Order Details:"+JSON.stringify($scope.orderDetails.address));
    };

    $scope.cashOnDeliveryPopup = function(){

        if($scope.addressValidation == 'true') {
            $('#cashOnDelivery').modal('show');
        }else {
            $scope.addressSelection = false;
        }
    };

    $scope.couponCheck = true;
    $scope.applyCoupon = function(){
            console.log("Coupon Code:"+$scope.orderDetails.couponCode);
        if($scope.couponCheck == true) {
            $scope.couponError = false;
            $scope.couponSuccess = false;
            $scope.couponItems = [];
            if ($scope.cartList) {
                for (var i = 0; i < $scope.cartList.length; i++) {
                    $scope.couponItems.push({
                        productId: $scope.cartList[i].id,
                        categoryId: $scope.cartList[i].cartItem.categoryId,
                        value: $scope.cartList[i].cartItem.price
                    })
                }
            }
            console.log("Coupon Items:" + JSON.stringify($scope.couponItems));
            $http({
                method: "POST",
                url: 'api/Coupons/applyCoupon',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
                data: {
                    "couponCode": $scope.orderDetails.couponCode,
                    "cartValue": $scope.orderDetails.cartValue,
                    "customerId": $scope.userInfo.id,
                    "products": $scope.couponItems
                }
            }).success(function (response) {
                console.log("Coupon Response:" + JSON.stringify(response));
                $scope.orderDetails.couponAmount = response.data.Coupon.discountAmount;
                $scope.orderDetails.cartValue = response.data.Coupon.finalValue;
                if($scope.orderDetails.cartValue <0){
                    $scope.orderDetails.cartValue = 0;
                }
                $scope.orderAmount = $scope.orderDetails.cartValue;
                $scope.orderDetails.couponStatus = "1";
                $scope.couponSuccess = true;
                $scope.couponCheck = false;
                $scope.couponSuccessMessage = "Coupon Applied Successfully";
            }).error(function (response) {
                console.log("Error Coupon:" + JSON.stringify(response));
                $scope.couponError = true;
                $scope.couponErrorMessage = response.message;
            });
        }else{
            $scope.couponSuccess = false;
            $scope.couponError = true;
            $scope.couponErrorMessage = "Coupon Already Applied";
        }
    };

    $scope.redeemCheck = true;
    $scope.loyaltyError = false;
    $scope.loyaltySuccess = false;
    $scope.redeemLoyalty = function () {
        if($scope.redeemCheck == true) {
            $scope.orderDetails.loyaltyPoint = $scope.userInfo.loyalty.totalPoints;
            $scope.orderDetails.loyaltyStatus = "1";
            $scope.userInfo.loyalty.totalPoints = 0;
            $scope.orderDetails.loyaltyAmount = $scope.orderDetails.loyaltyPoint / 100;
            $scope.orderAmount = $scope.orderAmount - $scope.orderDetails.loyaltyAmount;
            if($scope.orderAmount < 0){
                $scope.orderAmount = 0;
            }
            $scope.orderDetails.cartValue = $scope.orderAmount;
            $scope.redeemCheck = false;
            if($scope.orderDetails.loyaltyPoint > 0)
                $scope.loyaltySuccess = true;
            console.log("Loyalty Points:" + $scope.orderAmount);
        } else {
            $scope.loyaltyError = true;
            $scope.loyaltySuccess = false;
        }
    };

    $scope.placeOrder = function(paymentMode) {

        if($scope.orderDetails.deliveryCharges == 0){
            $scope.orderDetails.deliveryCharges = "Free";
        }else if($scope.orderDetails.deliveryCharges == 50){
            $scope.orderDetails.deliveryCharges = $scope.orderDetails.deliveryCharges.toFixed(2);
        }

        if( $scope.orderDetails.couponCode == ""){
            $scope.orderDetails.couponCode = "null";
        }

        if(paymentMode == 1){
            $scope.orderDetails.paymentMode = "CASH";
            $scope.orderDetails.paymentStatus = true;
            $scope.orderDetails.transactionId = "COD";
        }

        console.log("Order Details:"+JSON.stringify($scope.orderDetails.paymentMode));
        if($scope.addressValidation == 'true') {
            console.log("Entered...");
            $http({
                method: "POST",
                url: 'api/Orders',
                headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
                data: $scope.orderDetails
            }).success(function (response) {
                $window.localStorage.setItem('previousUrl','true');
                console.log("Response Order:" + JSON.stringify(response.data));
                $rootScope.orderSummeryCartList = $scope.orderDetails.cartItems;
                $rootScope.orderSummery =response.data;
                $window.localStorage.setItem('orderCartItems', JSON.stringify($rootScope.orderSummeryCartList));
                if(paymentMode == 1){
                    $('#cashOnDelivery').modal('hide');
                    $http({
                        method: "GET",
                        url: 'api/Customers/'+$scope.userInfo.id+'?access_token='+accessToken,
                        headers: {'Accept': 'application/json'}
                    }).success(function(data){
                        console.log("User Info after order:"+JSON.stringify(data.data));
                        $window.localStorage.setItem('userInfo', JSON.stringify(data.data));
                    }).error(function(data){
                        console.log("....................."+JSON.stringify(data));
                    });
                    $window.localStorage.removeItem('cartList');
                    $window.localStorage.removeItem('cartTotalAmount');
                    $window.localStorage.removeItem("cartCount");
                    $rootScope.cartCount = $window.localStorage.getItem("cartCount");
                    $location.url('/orderSummery');
                }else{
                    var userInfo = {};
                    userInfo['amount'] = $scope.orderDetails.cartValue;
                    userInfo['email'] = $scope.userInfo.email;
                    userInfo['phone'] = $scope.userInfo.phone;
                    userInfo['firstname'] = $scope.userInfo.name;
                    userInfo['productinfo'] = "Wellness";
                    userInfo['udf1']=$rootScope.orderSummery.id;
                    /*userInfo['udf4']=$scope.pkgName;
                    userInfo['udf5']=$scope.pkgBookDate;
                    userInfo['udf2']=$scope.vendorName;
                    userInfo['udf1']=$scope.packageduration;
                    userInfo['udf6']=$scope.time;*/
                    $window.location.href="http://"+$window.location.host+'/payURedirect';
                    var utd = {'id':$rootScope.orderSummery.id,'userInfo':userInfo};
                    $http({
                        method: 'POST',
                        url: '/addTransaction' ,
                        headers: {'Accept': 'application/json'},
                        data:  utd//forms user object
                    })
                        .success(function (data, status, headers, config, response) {
                            $window.location.href="http://"+$window.location.host+'/payURedirect?td='+$rootScope.orderSummery.id;
                        })
                        .error(function (data, status, headers, config) {
                            console.log(data);
                        });

                }

            }).error(function(response){
                console.log("Error Info:"+JSON.stringify(response));
            });
        }else{
            $scope.addressSelection = false;
            console.log("Please Select Address"); 
        }
    }
});

app.controller('reviewController', function ($http, $scope,$window,$location, $rootScope){
   console.log("Review Controller....");
    $rootScope.showReviewForm = false;

    $scope.userInfo = JSON.parse($window.localStorage.getItem("userInfo"));

    $scope.openReviewForm = function(){
        $rootScope.reviewErrorMessage = false;
        if($scope.userInfo) {
            $scope.rating = 1;
            $rootScope.reviewComment = '';
            $rootScope.showReviewForm = true;
        }else{
            $rootScope.reviewErrorMessage = true;
        }
    };

    $scope.redirectLogin = function(){
        $('#productPopUp').modal('hide');
        var url = $window.location.href;
        $window.localStorage.setItem("redirectUrl", url);
        $location.url('/login');
    };

    $scope.rateFunction = function(rating){
        console.log("Rating..."+rating);
    };

    $scope.addReview = function(id,comment, type){
        $scope.reviewData = {
            "rating": $scope.rating,
            "ratingFor": id,
            "createdBy": $scope.userInfo.id,
            "feedback": comment,
            "userName": $scope.userInfo.name,
            "ratingType": type
        };

        $http({
            method: "POST",
            url: 'api/Reviews',
            headers: {'Content-Type': 'application/json', 'Accept': 'application/json'},
            data: $scope.reviewData
        }).success(function (response) {
                console.log("Response Comment:"+JSON.stringify(response.data));
            $rootScope.showReviewForm = false;
            $scope.rating = 1;
            $rootScope.reviewComment = '';
            $http({
                method: "GET",
                url: 'api/Reviews?filter={"where":{"ratingFor":"'+response.data.ratingFor+'"}}',
                headers: {'Accept': 'application/json'}
            }).success(function(data){
                console.log("Review Data:"+JSON.stringify(data.data));
                $rootScope.productReview=[];
                $rootScope.productReview = data.data;
                $rootScope.reviewAvailable = true;
                if($rootScope.productReview.length == 0){
                    $rootScope.reviewAvailable = false;
                }
            });
        });

        $rootScope.reviewComment = '';
    };

    $scope.cancelReview = function(){
        $rootScope.showReviewForm = false;
        $scope.rating = 1;
        $rootScope.reviewComment = '';
    };
});

app.controller('payUCallBack', function ($http, $scope,$window,$location, $rootScope){
    console.log("Pay u controller");

    $scope.subId = function(oId, txnId, phone, name, amount){
        console.log("Pay u controller...........");
    };
});

app.filter('unique', function() {
    return function(collection, keyname) {
        var output = [],
            keys = [];

        angular.forEach(collection, function(item) {
            var key = item[keyname];
            if(keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });

        return output;
    };
});

app.directive('starRating', function() {
    return {
        restrict : 'A',
        template : '<ul class="rating">'
        + '	<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">'
        + '\u2605'
        + '</li>'
        + '</ul>',
        scope : {
            ratingValue : '=',
            max : '=',
            onRatingSelected : '&'
        },
        link : function(scope, elem, attrs) {
            var updateStars = function() {
                scope.stars = [];
                for ( var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled : i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function(index) {
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating : index + 1
                });
            };

            scope.$watch('ratingValue',
                function(oldVal, newVal) {
                    if (newVal) {
                        updateStars();
                    }
                }
            );
        }
    };
});