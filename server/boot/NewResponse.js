module.exports = function (server) {

    var remotes = server.remotes();
    // modify all returned values

    var Customer = server.models.Customer;
    var Store = server.models.Store;

    remotes.before('**', function(ctx, next){
        var Customer = server.models.Customer;
        console.log("actual method: "+ctx.req.originalUrl);
        if (ctx.req.originalUrl.toString().indexOf('/api/Customers/login') > -1 || ctx.req.originalUrl.toString().indexOf('/api/Stores/login') > -1){

            //console.log("after into: "+ JSON.stringify(ctx.req.body.phone));
            if (ctx.req.body.email == undefined) {
                //console.log("login");
                if(ctx.req.originalUrl.toString().indexOf('/api/Customers/login') > -1) {

                    //console.log("customer login");
                    Customer.find({"where":{"phone":ctx.req.body.phone}}, function(err, customers){

                        if(err) next(err);

                        if(customers.length > 0) {
                            console.log("found");
                            var customer = customers[0];
                            //console.log(customer.email);
                            ctx.req.body.email = customer.email;
                            //if(customer.otpStatus == false) {
                            //    var err = new Error("OTP is not verified");
                            //    next(err);
                            //} else {
                            //    next();
                            //}


                            next();
                        } else{
                            next();
                        }

                    });

                } else {

                    Store.find({"where":{"phone":ctx.req.body.phone}}, function(err, stores){

                        if(err) next(err);

                        if(stores.length > 0) {
                            var store = stores[0];
                            ctx.req.body.email = store.email;
                            //if(store.otpStatus == false) {
                            //    var err = new Error("OTP is not verified");
                            //    next(err);
                            //} else {
                            //    next();
                            //}
                            console.log(JSON.stringify(ctx.req.body));
                            next();
                        } else {
                            next();
                        }

                    });

                }

            } else {
                next();
            }

        } else if(((ctx.req.originalUrl.toString().indexOf('/api/Customers') > -1) || (ctx.req.originalUrl.toString().indexOf('/api/Stores') > -1)) && ctx.req.method.toString() == 'POST'){

            if(ctx.req.body.email == undefined) {
                ctx.req.body.email = ctx.req.body.phone + "@scoopy.com";
            }
            next();

        } else if((ctx.req.originalUrl.toString().indexOf('/api/Orders') > -1) && ctx.req.method.toString() == 'POST'){

            ctx.req.body.createdBy = ctx.req.body.customerId;
            ctx.req.body.createdTime = new Date();
            next();

        } else if((ctx.req.originalUrl.toString().indexOf('/api/Orders') > -1) && ctx.req.method.toString() == 'PUT'){

            ctx.req.body.updatedTime = new Date();
            next();

        } else if((ctx.req.originalUrl.toString().indexOf('/api/Favourites') > -1) && ctx.req.method.toString() == 'POST'){

            ctx.req.body.fuid = ctx.req.body.createdBy.toString() + ctx.req.body.favouriteItem.id.toString();
            next();

        } else {
            next();
        }
    });

    remotes.afterError('**', function(ctx,next) {
        ctx.result = {
            status:"failure",
            message:ctx.error.code,
            data: ctx.result
        };
        next();
    });

    remotes.after('**', function (ctx, next) {

        var Customer = server.models.Customer;
        var Address = server.models.Address;

        if(ctx.req.originalUrl.toString().indexOf('/userExists')> -1){

            if(ctx.result.customer != undefined) {
                console.log("customer Info:"+JSON.stringify(ctx.result.customer));
                if(ctx.result.customer.otpStatus == false){

                    ctx.result = {
                        status: "success",
                        message: "New User",
                        data: ctx.result
                    };
                    next();

                } else {

                    ctx.result = {
                        status: "success",
                        message: "Existed User",
                        data: ctx.result
                    };
                    next();

                }
            } else{
                if(ctx.result.store.otpStatus == false){

                    ctx.result = {
                        status: "success",
                        message: "New User",
                        data: ctx.result
                    };
                    next();

                } else {

                    ctx.result = {
                        status: "success",
                        message: "Existed User",
                        data: ctx.result
                    };
                    next();

                }
            }

        } else if (ctx.req.originalUrl.toString().indexOf('/api/Customers/login') > -1 || ctx.req.originalUrl.toString().indexOf('/api/Stores/login') > -1) {

            if(ctx.req.originalUrl.toString().indexOf('/api/Customers/login') > -1){

                console.log("data: "+JSON.stringify(ctx.result));
                var loginSuccessObjectInfo = ctx.result;
                var userId = loginSuccessObjectInfo.userId;

                Customer.findById(userId, function(err, customer){

                    if(err) {
                        next();
                    } else {
                        console.log("user: "+ JSON.stringify(customer));

                        if(customer == null || customer == undefined) {
                            ctx.result = loginSucc1essObjectInfo
                            ctx.result = {
                                status: "success",
                                message: "",
                                data: ctx.result
                            };
                            next();
                        } else {
                            customer.updateAttributes({"otpStatus":true}, function(err, customerUpdate){

                                Address.find({"where":{"customerId":userId}}, function(err, addresses){

                                    console.log(JSON.stringify(addresses));


                                    if(err){
                                        loginSuccessObjectInfo['customer'] = customerUpdate;
                                        ctx.result = loginSuccessObjectInfo;
                                        ctx.result = {
                                            status: "success",
                                            message: "",
                                            data: ctx.result
                                        };
                                        next();
                                    } else if(addresses.length > 0) {
                                        loginSuccessObjectInfo['customer'] = customerUpdate;
                                        loginSuccessObjectInfo['addresses'] = addresses;
                                        ctx.result = loginSuccessObjectInfo;
                                        ctx.result = {
                                            status: "success",
                                            message: "",
                                            data: ctx.result
                                        };
                                        next();
                                    } else {
                                        loginSuccessObjectInfo['customer'] = customerUpdate;
                                        ctx.result = loginSuccessObjectInfo;
                                        ctx.result = {
                                            status: "success",
                                            message: "",
                                            data: ctx.result
                                        };
                                        next();
                                    }


                                });
                            });
                        }

                    }

                });


            } else if(ctx.req.originalUrl.toString().indexOf('/api/Stores/login') > -1){

                console.log("data: "+JSON.stringify(ctx.result));
                var loginSuccessObjectInfo = ctx.result;
                var userId = loginSuccessObjectInfo.userId;

                Store.findById(userId, function(err, store){

                    if(err) {
                        next();
                    } else {
                        //console.log("user: "+ JSON.stringify(customer));

                        if(store == null || store == undefined) {
                            ctx.result = loginSucc1essObjectInfo;
                            ctx.result = {
                                status: "success",
                                message: "",
                                data: ctx.result
                            };
                            next();
                        } else {
                            store.updateAttributes({"otpStatus":true}, function(err, storeUpdate){

                                Address.find({"where":{"storeId":userId}}, function(err, addresses){

                                    console.log(JSON.stringify(addresses));


                                    if(err){
                                        loginSuccessObjectInfo['store'] = storeUpdate;
                                        ctx.result = loginSuccessObjectInfo;
                                        ctx.result = {
                                            status: "success",
                                            message: "",
                                            data: ctx.result
                                        };
                                        next();
                                    } else if(addresses.length > 0) {
                                        loginSuccessObjectInfo['store'] = storeUpdate;
                                        loginSuccessObjectInfo['addresses'] = addresses;
                                        ctx.result = loginSuccessObjectInfo;
                                        ctx.result = {
                                            status: "success",
                                            message: "",
                                            data: ctx.result
                                        };
                                        next();
                                    } else {
                                        loginSuccessObjectInfo['store'] = storeUpdate;
                                        ctx.result = loginSuccessObjectInfo;
                                        ctx.result = {
                                            status: "success",
                                            message: "",
                                            data: ctx.result
                                        };
                                        next();
                                    }


                                });
                            });
                        }

                    }

                });


            }

        } else if(ctx.req.originalUrl.toString().indexOf('/api/Items/customSearch') > -1){

            ctx.result = {
                status: "success",
                message: "",
                data: ctx.result.items
            };
            next();

        } else {
            ctx.result = {
                status: "success",
                message: "",
                data: ctx.result
            };
            next();
        }


    });

};
