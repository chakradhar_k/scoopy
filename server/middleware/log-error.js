/**
 * Created by Chakradhar on 2/17/2016.
 */

module.exports = function() {
    return function logError(err, req, res, next) {
        console.log('ERR', req.url, err);
    };
};
